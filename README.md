# README

**提供中文版以及英文版，請選擇您熟悉的語言進行閱讀**

**Available in both Chinese and English. Please select the language you are comfortable with.**

**選擇語言 / Choose Your Language**

- [中文版](README.md#中文版)
- [English Version](README.md#english-version)

# 中文版

# 專案介紹

## 描述

Music Share 是一個音樂分享平台，旨在讓使用者能夠輕鬆地上傳、分享和串流播放音樂。主要功能如下：

- **快速登入**：透過 Google 第三方登入，使用者可以快速建立帳號並開始使用平台。
- **音樂分享**：提供使用者上傳、分享和串流播放音樂的功能。使用者可以輕鬆地分享自己喜愛的音樂，並與朋友共同享受。
- **即時通知**：平台具備通知功能，使用者能夠即時收到關注的用戶新音樂發佈的通知，保持最新的音樂動態。

## Demo 網址

可以在以下網址查看 Music Share：

[https://musicshare.liaojourney.com](https://musicshare.liaojourney.com/)

**注意事項**：

- 由於沒有製作響應式網頁設計 (RWD)，請以 1500x1180 以上的解析度大小顯示器開啟，以獲得良好的瀏覽體驗。
- 開放時間：09:00 GMT+8 - 18:00 GMT+8

## Demo 影片

可以在以下網址查看 Music Share 平台的 Demo 影片：

[https://www.youtube.com/watch?v=Kjj0HvCsqtA](https://www.youtube.com/watch?v=Kjj0HvCsqtA)

## 相關 Repository

這些 Repository 包含了前端應用和雲端部署相關的所有檔案：

- **前端 Repository：**[https://gitlab.com/music-share66/frontend.git](https://gitlab.com/music-share66/frontend.git)
- **雲端 Repository：**[https://gitlab.com/music-share66/deploy.git](https://gitlab.com/music-share66/deploy.git)

# 選用技術

本專案所選用的前端、後端、資料庫、雲端等技術如下：

- **Frontend：React**
  - 使用 React 建構前端應用，提供使用者互動界面。
- **Backend：Express.js (TypeScript)**
  - 後端使用 Express.js 框架搭配 TypeScript 開發。
- **Database：PostgreSQL**
  - 選用 PostgreSQL 作為資料庫，提供強大的關聯數據管理和查詢功能。
- **Cache：Redis**
  - 使用 Redis 作為快取系統，提供快速數據存取，減少資料庫負載。
- **Storage：AWS S3**
  - 選用 AWS S3 存儲音樂文件。
- **Message Queue：Kafka**
  - 使用 Kafka 作為消息隊列系統，實現通知功能。
- **Test：Jest**
  - 使用 Jest 作為測試框架，提供單元測試和集成測試功能。
- **API Docs：Swagger**
  - 使用 Swagger 生成和管理 API 文件，提供直觀的 API 文件界面，方便開發者理解和使用 API。
- **Container：Docker, Kubernetes ( Helm )**
  - 使用 Docker 封裝應用，確保一致的運行環境。Kubernetes ( K8s ) 用於容器編排，Helm 用於管理 K8s 應用的部署，提供高效的資源管理和自動化部署能力。
- **CI / CD：GitLab CI**
  - 使用 GitLab CI 設置持續集成和持續部署流程。
- **IaC：Terraform**
  - 使用 Terraform 作為基礎設施即代碼 ( IaC ) 工具，管理和部署雲端資源。
- **Cloud：AWS**
  - 選用 AWS 作為雲服務平台，提供多種雲端服務和資源。

# 特點和亮點

Music Share 平台具備多項特點和亮點：

- **登入功能**：
  - 串接 Google 第三方登入，讓使用者能夠使用 Google 帳號快速建立和登入帳戶。這不僅簡化了註冊流程，還提高了安全性和可靠性，使用者無需再記住額外的登入憑證。
- **播放功能**：
  - 使用 HLS ( HTTP Live Streaming ) 協定實作音樂串流服務。HLS 通過將音樂文件分割成小片段並動態調整播放品質，實現更流暢的播放體驗。相比於直接從 storage 讀取播放檔案，HLS 降低了 50% 的傳輸資料附載，提升了傳輸效能和播放穩定性。
- **通知功能**：
  - 使用 Kafka 實現新音樂發布訂閱者通知功能。Kafka 可靠的消息傳遞系統能夠高效地處理大量通知請求。結合 WebSocket 技術，系統能夠實現即時通知，確保使用者在新音樂發布時能夠立即接收到相關通知，提升互動性。
- **部署流程**：
  - 使用 GitLab-CI 建立完整的 CI/CD 流程，實現代碼自動化測試、構建和部署。通過製作客製化的 Docker Image，優化了 EKS ( Elastic Kubernetes Service ) 和 Helm 的部署流程，確保應用的一致性和部署效率，並縮短發佈週期。
- **雲端架構**：
  - 使用 Terraform 於 AWS 搭建開發 ( development )、測試 ( staging ) 和生產 ( production ) 環境，模擬正式開發場景。相較於手動建立基礎設施，Terraform 自動化流程降低了 30% 的人為失誤，並提升了環境的一致性和可重現性。
  - 使用的服務包括 IAM 、VPC 、Route 53、ACM 、ECS、EKS、RDS、ElastiCache、S3、MediaConvert

# 架構

Music Share 專案採用了不同的架構方案來支持不同環境的運行，包括開發 ( development )、測試 ( staging ) 和生產 ( production ) 環境。

## 整體架構

為了實現 Music Share 的各項功能，選擇了以下元件組成整體架構：

- **Client**：用於上傳音樂和播放音樂。
- **Loadbalancer**：用於分流流量，確保系統穩定運行。
- **Server**：用於處理用戶請求和音樂檔案。
- **Cache**：用於加快API查詢速度。
- **Database**：用於存儲用戶和音樂的相關資料。
- **Storage**：用於存放處理過的音樂檔案。
- **Message Queue**：用於實現訂閱者通知功能。
- **Encoder**：用於將上傳的音樂檔案轉換為可串流播放的格式。

![High_level_design.png](./public/High_level_design.png)

## 開發環境 ( Development )

在開發環境中，Music Share 使用 ECS (Elastic Container Service) 來運行。

![Development_System_detailed_design.png](./public/Development_System_detailed_design.png)

## 測試環境 ( Staging )

在測試環境中，Music Share 使用 ECS 和 EKS ( Elastic Kubernetes Service ) 來運行。

![Staging_System_detailed_design.png](./public/Staging_System_detailed_design.png)

## 正式環境 ( Production )

在正式環境中，Music Share 也使用 ECS 和 EKS 來運行。這種架構保持了和測試環境相似的配置，確保了系統的一致性和穩定性。

![Production_System_detailed_design.png](./public/Production_System_detailed_design.png)

# Local 運行環境需求

在執行專案後端前，需要確保擁有以下運行環境：

- **Node.js 20.10.0**:
請確保安裝了 Node.js 20.10.0 或更高版本。可以從 [Node.js 官方網站](https://nodejs.org/) 下載並安裝適用的版本。

## 預請求

在本地運行專案後端前，確保以下所有資源和設置均已準備好，並正確配置相關連接資訊到環境變數檔 `.env` 中，這樣才能順利在本地運行此專案：

- **Postgres**： 安裝並配置 PostgreSQL 資料庫。需要創建一個資料庫並提供相應的連接資訊 (如：主機、端口、用戶名、密碼)。
- **Redis**： 安裝並配置 Redis。Redis 將用於快取數據，確保高效數據存取和減少資料庫負載。
- **Kafka Cluster**： 設置 Kafka 集群以實現消息隊列功能。Kafka 用於處理即時通知的功能。
- **AWS Account**： 需要一個 AWS 帳戶來使用雲端資源。
- **AWS S3 Bucket (Allow Public Access)**： 在 AWS 中創建一個 S3 Bucket，並配置為允許公開訪問。這將用於存儲和提供音樂文件。
- **AWS MediaConvert with Correct IAM Permission**： 配置 AWS MediaConvert 服務並賦予適當的 IAM 權限。MediaConvert 將用於轉換和處理音樂文件以適應不同的播放需求。
- **Google OAuth 2.0 Client ID**： 在 Google API Console 中創建 OAuth 2.0 憑證，獲取 Client ID，用於 Google 第三方登入功能。
- **Google OAuth 2.0 Client Secret**： 獲取與 Client ID 對應的 Client Secret，這些憑證將用於驗證使用者的 Google 登入請求。

## 環境檔 (.ENV)

為了讓專案後端在不同環境中順利運行，需要設定以下環境變數。這些環境變數在應用程式啟動時會被載入，並用於配置應用程式的各個方面，包括資料庫連接、API 認證、雲服務存取和消息隊列配置等。

請確保在 `.env` 文件中正確設置這些變數，以便應用程式能夠順利運行，請根據實際情況調整這些變數的值。
- **PORT**：應用程式運行的 port 號。
- **API_DOC_ENVIRONMENT**： 設定 API 文件的環境，如 `localhost`、`development`、`staging`、`production`。
- **ENVIRONMENT**： 應用程式運行的環境，如 `localhost`、`development`、`staging`、`production`。
- **CLIENT_HOST**： 前端應用的主機 Domain。
- **DB_TYPE**： 資料庫類型 (例如 `postgres`)。
- **DB_HOST**： 資料庫伺服器的主機地址。
- **DB_PORT**： 資料庫伺服器的 Port 號。
- **DB_USERNAME**： 連接資料庫的用戶名。
- **DB_PASSWORD**： 連接資料庫的密碼。
- **DB_DATABASE**： 資料庫名稱。
- **JWT_SECRET_KEY**： 用於 JWT (JSON Web Token) 驗證的密鑰。
- **GOOGLE_CLIENT_ID**： Google OAuth 2.0 的 Client ID，用於第三方登入。
- **GOOGLE_CLIENT_SECRET**： Google OAuth 2.0 的 Client Secret，用於第三方登入。
- **GOOGLE_OAUTH_URL**： Google OAuth 2.0 的授權 URL。
- **GOOGLE_CALL_BACK_URL**： Google OAuth 2.0 的回調 URL。
- **GOOGLE_ACCESS_TOKEN_URL**： Google OAuth 2.0 的存取令牌 URL。
- **GOOGLE_TOKEN_INFO_URL**： 用於獲取 Google OAuth 2.0 存取令牌資訊的 URL。
- **AWS_ACCESS_KEY**： AWS 存取密鑰 ID，用於存取 AWS 服務。
- **AWS_ACCESS_SECRET**： AWS 存取密鑰，用於存取 AWS 服務。
- **AWS_REGION**： AWS 服務所使用的區域。
- **AWS_BUCKET_NAME**： 存儲音樂文件的 AWS S3 Bucket 名稱。
- **KAFKA_BROKER**： Kafka Broker 的地址。
- **KAFKA_USERNAME**： 連接 Kafka 的用戶名。
- **KAFKA_PASSWORD**： 連接 Kafka 的密碼。
- **REDIS_HOST**： Redis 伺服器的主機地址。

以下是 `.env` 文件的範例格式：
```
PORT=3000
API_DOC_ENVIRONMENT=development
ENVIRONMENT=development
CLIENT_HOST=http://localhost:3000
DB_TYPE=postgres
DB_HOST=localhost
DB_PORT=5432
DB_USERNAME=your_db_username
DB_PASSWORD=your_db_password
DB_DATABASE=your_db_name
JWT_SECRET_KEY=your_jwt_secret_key
GOOGLE_CLIENT_ID=your_google_client_id
GOOGLE_CLIENT_SECRET=your_google_client_secret
GOOGLE_OAUTH_URL=https://accounts.google.com/o/oauth2/auth
GOOGLE_CALL_BACK_URL=http://localhost:3000/auth/google/callback
GOOGLE_ACCESS_TOKEN_URL=https://oauth2.googleapis.com/token
GOOGLE_TOKEN_INFO_URL=https://www.googleapis.com/oauth2/v1/tokeninfo
AWS_ACCESS_KEY=your_aws_access_key
AWS_ACCESS_SECRET=your_aws_access_secret
AWS_REGION=your_aws_region
AWS_BUCKET_NAME=your_aws_bucket_name
KAFKA_BROKER=your_kafka_broker
KAFKA_USERNAME=your_kafka_username
KAFKA_PASSWORD=your_kafka_password
REDIS_HOST=your_redis_host
```

## 在 Loacl 安裝, 運行

按照以下步驟在本地環境中安裝並運行專案後端：

**Step 1. 安裝套件**：

這個指令會根據 package.json 安裝所有必要的 npm 套件。

```
npm install
```
    
**Step 2. 執行資料庫 Migration**：

這個指令會根據 TypeORM 的設定進行資料庫 Migration，確保資料庫結構與應用程式一致。

```
npm run migration:dev:run
```
    
**Step 3. 啟動開發環境**：

這個指令會啟動開發伺服器，您可以在瀏覽器中打開 `http://localhost:3000` 來訪問應用程式。

```
npm run dev
``` 

## 測試

為確保應用程式的質量和穩定性，可以運行以下指令進行測試：

**執行測試**：

這個指令會運行所有定義的單元測試和集成測試。測試框架使用 Jest，並生成測試報告。

```
npm run test
```

**測試覆蓋率報告**

![Unit_test.png](./public/Unit_test.png)  

## API 文件

為了方便開發者了解和使用 API，在 local 開發環境中，專案提供了自動生成的 Swagger API 文件，提供所有 API 的詳細資訊，包括路由、請求參數和回應格式。

可以通過以下網址訪問 API 文件：
    
http://localhost:3000/api-docs

**Swagger API 文件**：

![API_docs.png](./public/API_docs.png)

# English Version

# Project Introduction

## Description

Music Share is a music-sharing platform designed to allow users to easily upload, share, and stream music. The main features are as follows:

- **Quick Login*： Users can quickly create an account and start using the platform through Google third-party login.
- **Music Sharing** : Provides users with the ability to upload, share, and stream music. Users can easily share their favorite music and enjoy it with friends.
- **Real-Time Notifications** : The platform has a notification function that allows users to receive immediate notifications of new music releases from followed users, keeping them updated with the latest music trends.

## Demo URL

You can view the demo of Music Share at the following URL : 

https://musicshare.liaojourney.com

**Note** :

- As there is no responsive web design (RWD), please open it on a display with a resolution of 1500x1180 or above for a better browsing experience.
- Available time: 09:00 GMT+8 - 18:00 GMT+8

## Demo Video

You can view the demo video of the Music Share platform at the following URL :

[https://www.youtube.com/watch?v=Kjj0HvCsqtA](https://www.youtube.com/watch?v=Kjj0HvCsqtA)

## Related Repositories

These repositories contain all files related to the frontend application and cloud deployment :

- **Frontend Repository** : [https://gitlab.com/music-share66/frontend.git](https://gitlab.com/music-share66/frontend.git)
- **Cloud Repository** : [https://gitlab.com/music-share66/deploy.git](https://gitlab.com/music-share66/deploy.git)

# Technology Stack

The technologies used for the frontend, backend, database, and cloud in this project are as follows :

- **Frontend : React**
  - Using React to build the frontend application, providing a user-interactive interface.
- **Backend : Express.js (TypeScript)**
  - The backend is developed using the Express.js framework with TypeScript.
- **Database : PostgreSQL**
  - PostgreSQL is chosen as the database, providing powerful relational data management and query capabilities.
- **Cache : Redis**
  - Redis is used as a caching system to provide fast data access and reduce database load.
- **Storage : AWS S3**
  - AWS S3 is used for storing music files.
- **Message Queue : Kafka**
  - Kafka is used as the message queue system to implement the notification function.
- **Test : Jest**
  - Jest is used as the testing framework to provide unit testing and integration testing capabilities.
- **API Docs : Swagger**
  - Swagger is used to generate and manage API documentation, providing an intuitive API documentation interface for developers to understand and use the API.
- **Container : Docker, Kubernetes ( Helm )**
  - Docker is used to package applications to ensure a consistent running environment. Kubernetes (K8s) is used for container orchestration, and Helm is used to manage K8s application deployments, providing efficient resource management and automated deployment capabilities.
- **CI / CD : GitLab CI**
  - GitLab CI is used to set up continuous integration and continuous deployment processes.
- **IaC : Terraform**
  - Terraform is used as the infrastructure as code (IaC) tool to manage and deploy cloud resources.
- **Cloud : AWS**
  - AWS is chosen as the cloud service platform, providing various cloud services and resources.

# Features and Highlights

The Music Share platform has multiple features and highlights :

- **Login Functionality** : 
  - Integrates Google third-party login to allow users to quickly create and log in to their accounts using their Google account. This not only simplifies the registration process but also enhances security and reliability, as users don't need to remember additional login credentials.
- **Playback Functionality** : 
  - Implements music streaming service using HLS (HTTP Live Streaming) protocol. HLS achieves smoother playback experience by splitting music files into small segments and dynamically adjusting playback quality. Compared to directly reading playback files from storage, HLS reduces transmission data load by 50%, improving transmission efficiency and playback stability.
- **Notification Functionality** : 
  - Implements subscriber notification functionality for new music releases using Kafka. Kafka's reliable message delivery system can efficiently handle a large number of notification requests. Combined with WebSocket technology, the system can achieve real-time notifications, ensuring users receive relevant notifications immediately upon new music release, enhancing interactivity.
- **Deployment Process** : 
  - Establishes a complete CI/CD process using GitLab CI to achieve automated code testing, building, and deployment. By creating customized Docker Images, the deployment process of EKS (Elastic Kubernetes Service) and Helm is optimized, ensuring application consistency and deployment efficiency, and shortening the release cycle.
- **Cloud Architecture** : 
  - Uses Terraform to set up development, testing, and production environments on AWS, simulating real development scenarios. Compared to manually setting up infrastructure, Terraform's automated process reduces human errors by 30% and improves environment consistency and reproducibility.
  - Services used include IAM, VPC, Route 53, ACM, ECS, EKS, RDS, ElastiCache, S3, MediaConvert.

# Architecture

The Music Share project adopts different architecture schemes to support the operation in different environments, including development, testing, and production environments.

## Overall Architecture

To achieve the various functions of Music Share, the following components are selected to form the overall architecture :

- **Client** : Used for uploading and playing music.
- **Loadbalancer** : Used for traffic distribution to ensure stable system operation.
- **Server** : Used for handling user requests and music files.
- **Cache** : Used to accelerate API query speed.
- **Database** : Used for storing user and music-related data.
- **Storage** : Used for storing processed music files.
- **Message Queue** : Used for implementing subscriber notification functionality.
- **Encoder** : Used for converting uploaded music files to a streamable format.

![High_level_design.png](./public/High_level_design.png)

## Development Environment

In the development environment, Music Share runs using ECS (Elastic Container Service).

![Development_System_detailed_design.png](./public/Development_System_detailed_design.png)

## Testing Environment

In the testing environment, Music Share runs using ECS and EKS (Elastic Kubernetes Service).

![Staging_System_detailed_design.png](./public/Staging_System_detailed_design.png)

## Production Environment

In the production environment, Music Share also runs using ECS and EKS. This architecture maintains a similar configuration to the testing environment, ensuring system consistency and stability.

![Production_System_detailed_design.png](./public/Production_System_detailed_design.png)

# Local Running Environment Requirements

Before running the project backend, ensure that you have the following running environment :

- **Node.js 20.10.0** :
Please ensure that you have installed Node.js 20.10.0 or higher. You can download and install the appropriate version from the Node.js official website.

## Prerequisites

Before running the project backend locally, ensure that all the following resources and settings are prepared and correctly configured in the environment variable file .env so that the project can run smoothly locally :

- **Postgres** :  Install and configure PostgreSQL database. You need to create a database and provide the corresponding connection information (such as host, port, username, password).
- **Redis** :  Install and configure Redis. Redis will be used for caching data, ensuring efficient data access and reducing database load.
- **Kafka Cluster** :  Set up a Kafka cluster to implement the message queue function. Kafka is used for handling real-time notifications.
- **AWS Account** :  An AWS account is needed to use cloud resources.
- **AWS S3 Bucket (Allow Public Access)** :  Create an S3 bucket in AWS and configure it to allow public access. This will be used for storing and serving music files.
- **AWS MediaConvert with Correct IAM Permission** :  Configure AWS MediaConvert service and assign appropriate IAM permissions. MediaConvert will be used to convert and process music files to meet different playback needs.
- **Google OAuth 2.0 Client ID** :  Create OAuth 2.0 credentials in the Google API Console and obtain the Client ID for Google third-party login functionality.
- **Google OAuth 2.0 Client Secret** :  Obtain the Client Secret corresponding to the Client ID. These credentials will be used to authenticate users' Google login requests.

## Environment File (.ENV)

To ensure the project backend runs smoothly in different environments, the following environment variables need to be set. These environment variables will be loaded when the application starts and used to configure various aspects of the application, including database connections, API authentication, cloud service access, and message queue configuration.

Make sure to set these variables correctly in the .env file to ensure the application runs smoothly. Adjust the values of these variables according to your actual situation.
- **PORT** : The port number on which the application runs.
- **API_DOC_ENVIRONMENT** :  Set the API documentation environment, such as localhost, development, staging, production.
- **ENVIRONMENT** : The environment in which the application runs, such as localhost, development, staging, production.
- **CLIENT_HOST** : The domain of the frontend application.
- **DB_TYPE** : The type of database (e.g., postgres).
- **DB_HOST** : The host address of the database server.
- **DB_PORT** : The port number of the database server.
- **DB_USERNAME** : The username for connecting to the database.
- **DB_PASSWORD** : The password for connecting to the database.
- **DB_DATABASE** : The name of the database.
- **JWT_SECRET_KEY** : The secret key used for JWT (JSON Web Token) authentication.
- **GOOGLE_CLIENT_ID** : Google OAuth 2.0 Client ID for third-party login.
- **GOOGLE_CLIENT_SECRET** : Google OAuth 2.0 Client Secret for third-party login.
- **GOOGLE_OAUTH_URL** : The authorization URL for Google OAuth 2.0.
- **GOOGLE_CALL_BACK_URL** : The callback URL for Google OAuth 2.0.
- **GOOGLE_ACCESS_TOKEN_URL** : The access token URL for Google OAuth 2.0.
- **GOOGLE_TOKEN_INFO_URL** : The URL for obtaining token information for Google OAuth 2.0.
- **AWS_ACCESS_KEY** : The AWS access key ID, used to access AWS services.
- **AWS_ACCESS_SECRET** : The AWS secret access key, used to access AWS services.
- **AWS_REGION** : The region used for AWS services.
- **AWS_BUCKET_NAME** : The name of the AWS S3 bucket used to store music files.
- **KAFKA_BROKER** : The address of the Kafka broker.
- **KAFKA_USERNAME** : The username for connecting to Kafka.
- **KAFKA_PASSWORD** : The password for connecting to Kafka.
- **REDIS_HOST** : The host address of the Redis server.

`.env` file example : 
```
PORT=3000
API_DOC_ENVIRONMENT=development
ENVIRONMENT=development
CLIENT_HOST=http://localhost:3000
DB_TYPE=postgres
DB_HOST=localhost
DB_PORT=5432
DB_USERNAME=your_db_username
DB_PASSWORD=your_db_password
DB_DATABASE=your_db_name
JWT_SECRET_KEY=your_jwt_secret_key
GOOGLE_CLIENT_ID=your_google_client_id
GOOGLE_CLIENT_SECRET=your_google_client_secret
GOOGLE_OAUTH_URL=https://accounts.google.com/o/oauth2/auth
GOOGLE_CALL_BACK_URL=http://localhost:3000/auth/google/callback
GOOGLE_ACCESS_TOKEN_URL=https://oauth2.googleapis.com/token
GOOGLE_TOKEN_INFO_URL=https://www.googleapis.com/oauth2/v1/tokeninfo
AWS_ACCESS_KEY=your_aws_access_key
AWS_ACCESS_SECRET=your_aws_access_secret
AWS_REGION=your_aws_region
AWS_BUCKET_NAME=your_aws_bucket_name
KAFKA_BROKER=your_kafka_broker
KAFKA_USERNAME=your_kafka_username
KAFKA_PASSWORD=your_kafka_password
REDIS_HOST=your_redis_host
```

## Installing and Running Locally

Follow these steps to install and run the backend of the project in your local environment :

**Step 1. Install Dependencies** : 

This command will install all necessary npm packages according to the package.json file.

```
npm install
```
    
**Step 2. Run Database Migrations** : 

This command will execute database migrations based on the TypeORM configuration to ensure the database schema is consistent with the application.

```
npm run migration:dev:run
```
    
**Step 3. Start Development Server** : 

This command will start the development server. You can access the application in your browser at http://localhost:3000.

```
npm run dev
``` 

## Testing

To ensure the quality and stability of the application, you can run the following commands for testing :

**Run Tests** : 

This command will execute all defined unit tests and integration tests. The testing framework used is Jest, and it will generate a test report.

```
npm run test
```

**Test Coverage Report**

![Unit_test.png](./public/Unit_test.png)  

## API Documentation

To facilitate developers in understanding and using the API, the project provides auto-generated Swagger API documentation in the local development environment. It includes detailed information about all APIs, including routes, request parameters, and response formats.

You can access the API documentation at the following URL : 
    
http://localhost:3000/api-docs

**Swagger API Documentation** : 

![API_docs.png](./public/API_docs.png)
