This is custom docker image for gitlab-ci deploy to eks.

This Dockerfile conatin awscli, kubectl, helm.

Run this command to build image and push to your own image registry.
$ docker build -t awscli-kubectl-helm:latest .
$ docker login
$ docker push <your registry>/awscli-kubectl-helm:latest

After that, modify .gitlab-ci.yml eks_deploy_template change it's image to the image you just builded

And now you can now use this command to update your eks cluster easily
aws --profile default configure set aws_access_key_id $YOUR_AWS_ACCESS_KEY_ID
aws --profile default configure set aws_secret_access_key $YOUR_AWS_SECRET_ACCESS_KEY
aws eks update-kubeconfig --name $EKS_CLUSTER_NAME --region $EKS_CLUSTER_REGION