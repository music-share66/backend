stages:
  - test
  - build
  - deploy

# Define test, build and deploy templates
.lint_test_template:
  image: node:20-alpine
  script:
    - npm install
    - npm run lint

.unit_test_template:
  image: node:20-alpine
  variables:
    - APP_ENV: ""
  before_script:
    - chmod +x $APP_ENV
    - source $APP_ENV
  script:
    - npm install
    - npm run test

.build_template:
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    IMAGE_TAG_LATEST: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-latest
  script:
    - echo "docker bulid and release, ${CI_COMMIT_REF_SLUG}, ${FRONTEND_DOMAIN}, ${BACKEND_DOMAIN}, ${IMAGE_TAG}, ${IMAGE_TAG_LATEST}"
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
    - docker tag $IMAGE_TAG $IMAGE_TAG_LATEST
    - docker push $IMAGE_TAG_LATEST

.ecs_deploy_template:
  image:
    name: amazon/aws-cli
    entrypoint: [""]
  variables:
    CI_AWS_ECS_CLUSTER: ""
    CI_AWS_ECS_SERVICE: ""
    CI_AWS_ECS_TASK_DEFINITION: ""
  script:
    - echo "Deploying to ECS, ${CI_AWS_ECS_CLUSTER}, ${CI_AWS_ECS_SERVICE}, ${CI_AWS_ECS_TASK_DEFINITION}"
    - aws ecs update-service --cluster $CI_AWS_ECS_CLUSTER --service $CI_AWS_ECS_SERVICE --task-definition $CI_AWS_ECS_TASK_DEFINITION --force-new-deployment

.eks_deploy_template:
  image: liaoliao5278/awscli-kubectl-helm:latest
  variables:
    - EKS_CLUSTER_NAME: ""
    - EKS_CLUSTER_REGION: ""
    - APP_ENV: ""
    - HELM_IMAGE: ""
    - HELM_BACKEND_DOMAIN: ""
    - HELM_EXTERNAL_NAME: ""
    - HELM_SERVCE_PORT: ""
  before_script:
    - aws --profile default configure set aws_access_key_id $AWS_ACCESS_KEY_ID
    - aws --profile default configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    - aws eks update-kubeconfig --name $EKS_CLUSTER_NAME --region $EKS_CLUSTER_REGION
    - chmod +x $APP_ENV
    - source $APP_ENV
    - export HELM_IMAGE=$HELM_IMAGE
    - export HELM_BACKEND_DOMAIN=$HELM_BACKEND_DOMAIN
    - export HELM_EXTERNAL_NAME=$HELM_EXTERNAL_NAME
    - export HELM_SERVCE_PORT=$HELM_SERVCE_PORT
    - envsubst < ./helm/values_template.yaml > ./helm/values.yaml
  script:
    - echo "Deploy to eks, ${EKS_CLUSTER_NAME}, ${EKS_CLUSTER_REGION}"
    - kubectl get nodes
    - helm upgrade --install musicshare-backend ./helm --force

# dev
dev_lint_test:
  stage: test
  extends: .lint_test_template
  only:
    - dev

dev_unit_test:
  stage: test
  extends: .unit_test_template
  variables:
    APP_ENV: $DEV_APP_ENV
  only:
    - dev

dev_release_docker_image:
  stage: build
  needs:
    - dev_lint_test
    - dev_unit_test
  extends: .build_template
  only:
    - dev

dev_ecs_deploy:
  stage: deploy
  needs:
    - dev_release_docker_image
  extends: .ecs_deploy_template
  variables:
    CI_AWS_ECS_CLUSTER: $DEV_CI_AWS_ECS_CLUSTER
    CI_AWS_ECS_SERVICE: $DEV_CI_AWS_ECS_SERVICE
    CI_AWS_ECS_TASK_DEFINITION: $DEV_CI_AWS_ECS_TASK_DEFINITION
  only:
    - dev

# stg
stg_lint_test:
  stage: test
  extends: .lint_test_template
  only:
    - stg

stg_unit_test:
  stage: test
  extends: .unit_test_template
  variables:
    APP_ENV: $STG_APP_ENV
  only:
    - stg

stg_release_docker_image:
  stage: build
  needs:
    - stg_lint_test
    - stg_unit_test
  extends: .build_template
  only:
    - stg

stg_eks_deploy:
  stage: deploy
  needs:
    - stg_release_docker_image
  extends: .eks_deploy_template
  variables:
    EKS_CLUSTER_NAME: $STG_EKS_CLUSTER_NAME
    EKS_CLUSTER_REGION: $STG_EKS_CLUSTER_REGION
    APP_ENV: $STG_APP_ENV
    HELM_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-latest
    HELM_BACKEND_DOMAIN: $STG_HELM_BACKEND_DOMAIN
    HELM_EXTERNAL_NAME: $STG_HELM_EXTERNAL_NAME
    HELM_SERVCE_PORT: $STG_HELM_SERVCE_PORT
  only:
    - stg

# prod
prod_lint_test:
  stage: test
  extends: .lint_test_template
  only:
    - prod

prod_unit_test:
  stage: test
  extends: .unit_test_template
  variables:
    APP_ENV: $PROD_APP_ENV
  only:
    - prod

prod_release_docker_image:
  stage: build
  needs:
    - prod_lint_test
    - prod_unit_test
  extends: .build_template
  only:
    - prod

prod_eks_deploy:
  stage: deploy
  needs:
    - prod_release_docker_image
  extends: .eks_deploy_template
  variables:
    EKS_CLUSTER_NAME: $PROD_EKS_CLUSTER_NAME
    EKS_CLUSTER_REGION: $PROD_EKS_CLUSTER_REGION
    APP_ENV: $PROD_APP_ENV
    HELM_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-latest
    HELM_BACKEND_DOMAIN: $PROD_HELM_BACKEND_DOMAIN
    HELM_EXTERNAL_NAME: $PROD_HELM_EXTERNAL_NAME
    HELM_SERVCE_PORT: $PROD_HELM_SERVCE_PORT
  when: manual
  only:
    - prod
