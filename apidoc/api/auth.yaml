paths:
  /signin/user:
    get:
      tags:
        - Auth
      summary: Get signin user
      security:
        - cookieAuth: []
      responses:
        '200':
          description: Get signin user successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
                example:
                  message: 'User logged in successfully'
                  status: 'success'
                  result: { userId: '<userId>', userName: '<userName>', following: '[{followingUser}]'}
        '401':
          description: Get signin user fail
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UnAuthorizedError'
                example:
                  message: 'Authorization token is required | Invalid token | <error message>'
                  status: 'error'
                  statusCode: 401
        '500':
          description: InternalServerError
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InternalServerError'
                example:
                  message: '<error message>'
                  status: 'error'
                  statusCode: 500
  /google/signin:
    get:
      tags:
        - Auth
      summary: Get google consent screen redirest url
      responses:
        '200':
          description: Get google consent screen redirest url successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
                example:
                  message: 'Get redirectUrl successfully'
                  status: 'success'
                  result: '<google consent screen redirest url>'
        '500':
          description: InternalServerError
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InternalServerError'
                example:
                  message: '<error message>'
                  status: 'error'
                  statusCode: 500
  /google/callback:
    get:
      tags:
        - Auth
      summary: Google OAuth Callback
      description: This endpoint is used to handle the callback from Google OAuth, exchange the authorization code for tokens, and set the JWT token in a cookie for the user.
      parameters:
        - in: query
          name: code
          schema:
            type: string
          required: true
          description: The authorization code received from Google OAuth.
      responses:
        '301':
          description: Redirects to the client host with the JWT token set in a cookie.
          headers:
            Set-Cookie:
              schema:
                type: string
                example: Authorization=Bearer <JWT_TOKEN>; HttpOnly; Max-Age=3600
        '401':
          description: Google sign in fail
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UnAuthorizedError'
                example:
                  message: '<error message>'
                  status: 'error'
                  statusCode: 401
        '500':
          description: InternalServerError
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InternalServerError'
                example:
                  message: '<error message>'
                  status: 'error'
                  statusCode: 500
  /signup:
    post:
      tags:
        - Auth
      summary: Register a new user
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                email:
                  type: string
                  format: email
                  description: The email of the user
                password:
                  type: string
                  description: The password of the user
                repeatPassword:
                  type: string
                  description: The repeat password of the password
                userName:
                  type: string
                  description: The username of the user
              required:
                - email
                - password
                - userName
              example:
                email: example@gmail.com
                password: password
                repeatPassword: password
                userName: example
      responses:
        '201':
          description: User created successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
                example:
                  message: 'User created successfully'
                  status: 'success'
                  result: '<user name>'
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestError'
                example:
                  message: 'Invalid request payload input'
                  status: 'error'
                  statusCode: 400
        '409':
          description: Conflict
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ConflictError'
                example:
                  message: 'Email already exist'
                  status: 'error'
                  statusCode: 409
        '500':
          description: InternalServerError
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InternalServerError'
                example:
                  message: '<error message>'
                  status: 'error'
                  statusCode: 500
  /signin:
    post:
      tags:
        - Auth
      summary: Sign in
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                email:
                  type: string
                  format: email
                  description: The email of the user
                password:
                  type: string
                  description: The password of the user
              required:
                - email
                - password
              example:
                email: example@gmail.com
                password: password
      responses:
        '200':
          description: User sign in successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
                example:
                  message: 'User logged in successfully'
                  status: 'success'
                  result: 'success'
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestError'
                example:
                  message: 'Invalid request payload input'
                  status: 'error'
                  statusCode: 400
        '401':
          description: User sign in fail
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UnAuthorizedError'
                example:
                  message: 'Invalid email or password'
                  status: 'error'
                  statusCode: 401
        '500':
          description: InternalServerError
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InternalServerError'
                example:
                  message: '<error message>'
                  status: 'error'
                  statusCode: 500
  /signout:
    post:
      tags:
        - Auth
      summary: Sign out a user
      description: This endpoint is used to sign out a user by clearing the Authorization cookie.
      responses:
        '200':
          description: User logged out successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
                example:
                  message: 'User logged out successfully'
                  status: 'success'
                  result: 'success'
        '500':
          description: InternalServerError
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InternalServerError'
                example:
                  message: '<error message>'
                  status: 'error'
                  statusCode: 500
