paths:
  /songs/{songId}/m3u8file-url:
    get:
      tags:
        - Song
      summary: Get m3u8 file url
      parameters:
        - name: songId
          in: path
          required: true
          description: Song ID
          schema:
            type: string
      responses:
        '200':
          description: Get Song m3u8 file url successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
                example:
                  message: 'Get Song m3u8 file url successfully'
                  status: 'success'
                  result: 'https://example/songs/1/playlisthls.m3u8'
        '404':
          description: Song does not exist
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/NotFoundError'
                example:
                  message: 'Song does not exist'
                  status: 'error'
                  statusCode: 404
        '500':
          description: InternalServerError
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InternalServerError'
                example:
                  message: '<error message>'
                  status: 'error'
                  statusCode: 500
  /songs:
    post:
      tags:
        - Song
      summary: Upload and process a new song
      security:
        - cookieAuth: []
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                songFile:
                  type: string
                  format: binary
                songName:
                  type: string
              required:
                - songFile
                - songName
      responses:
        '202':
          description: Accepted, New song creating
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
                example:
                  message: 'New song creating'
                  status: 'success'
                  result: 'success'
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestError'
                example:
                  message: 'Invalid request payload input'
                  status: 'error'
                  statusCode: 400
        '401':
          description: Get signin user fail
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UnAuthorizedError'
                example:
                  message: 'Authorization token is required | Invalid token | <error message>'
                  status: 'error'
                  statusCode: 401
        '404':
          description: User does not exist
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/NotFoundError'
                example:
                  message: 'User does not exist'
                  status: 'error'
                  statusCode: 404
        '500':
          description: InternalServerError
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InternalServerError'
                example:
                  message: 'Failed to upload song | Failed to encode song'
                  status: 'error'
                  statusCode: 500
  /songs/{songId}:
    delete:
      tags:
        - Song
      summary: Delete a song
      security:
        - cookieAuth: []
      parameters:
        - name: songId
          in: path
          required: true
          description: Song ID
          schema:
            type: string
      responses:
        '202':
          description: Accepted, Deleting song
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
                example:
                  message: 'Song deleting'
                  status: 'success'
                  result: 'success'
        '401':
          description: Get signin user fail
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UnAuthorizedError'
                example:
                  message: 'Authorization token is required | Invalid token | <error message>'
                  status: 'error'
                  statusCode: 401
        '403':
          description: Unauthorized to delete a song
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ForbiddenError'
                example:
                  message: 'Have no authorized to delete this resource.'
                  status: 'error'
                  statusCode: 403
        '404':
          description: Song does not exist
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/NotFoundError'
                example:
                  message: 'Song does not exist | Song files does not exist'
                  status: 'error'
                  statusCode: 404
        '500':
          description: InternalServerError
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InternalServerError'
                example:
                  message: '<error message>'
                  status: 'error'
                  statusCode: 500
  /songs/encode-status:
    put:
      tags:
        - Song
      summary: Update songs process status
      security:
        - cookieAuth: []
      responses:
        '202':
          description: Accepted, Updating songs process status
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
                example:
                  message: 'Checking HLS encoder'
                  status: 'success'
                  result: 'success'
        '401':
          description: Get signin user fail
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UnAuthorizedError'
                example:
                  message: 'Authorization token is required | Invalid token | <error message>'
                  status: 'error'
                  statusCode: 401
        '404':
          description: User does not exist
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/NotFoundError'
                example:
                  message: 'User does not exist'
                  status: 'error'
                  statusCode: 404
        '500':
          description: InternalServerError
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InternalServerError'
                example:
                  message: '<error message>'
                  status: 'error'
                  statusCode: 500
