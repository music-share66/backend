paths:
  /users/name/{userName}:
    get:
      tags:
        - User
      summary: Get user by name
      parameters:
        - in: path
          name: userName
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Get user by name successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
                example:
                  message: 'Get user by name successfully'
                  status: 'success'
                  result: [
                    {
                      id: '<userId>',
                      userName: '<userName>',
                    }
                  ]
        '500':
          description: InternalServerError
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InternalServerError'
                example:
                  message: '<error message>'
                  status: 'error'
                  statusCode: 500
  /users/{userId}/songs:
    get:
      tags:
        - User
      summary: Get user songs
      parameters:
        - in: path
          name: userId
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Get user's songs successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
                example:
                  message: 'Get user songs successfully'
                  status: 'success'
                  result: { 
                    id: '<userId>',
                    userName: '<userName>',
                    songs: [
                      {
                        id: '<songId>',
                        songName: '<songName>',
                        durationInMs: '<durationInMs>',
                        encodedSongFileUrl: '<encodedSongFileUrl>',
                        processingStatus: '<processingStatus>'
                      }
                    ]
                  }
        '500':
          description: InternalServerError
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InternalServerError'
                example:
                  message: '<error message>'
                  status: 'error'
                  statusCode: 500
  /users/{userId}/following:
    post:
      tags:
        - User
      summary: Follow a user
      security:
        - cookieAuth: []
      parameters:
        - in: path
          name: userId
          schema:
            type: string
          required: true
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                followeeId:
                  type: string
                  description: The user id to follow
                  example: '<followeeId>'
      responses:
        '200':
          description: Follow user successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
                example:
                  message: 'Follow user successfully'
                  status: 'success'
                  result: 'success'
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UnAuthorizedError'
                example:
                  message: 'Authorization token is required | Invalid token | <error message>'
                  status: 'error'
                  statusCode: 401
        '403':
          description: Forbidden
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ForbiddenError'
                example:
                  message: 'Have no authorized to follow user.'
                  status: 'error'
                  statusCode: 403
        '404':
          description: NotFound
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/NotFoundError'
                example:
                  message: 'User does not exist'
                  status: 'error'
                  statusCode: 404
        '500':
          description: InternalServerError
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InternalServerError'
                example:
                  message: '<error message>'
                  status: 'error'
                  statusCode: 500
  /users/{userId}/following/{followingId}:
    delete:
      tags:
        - User
      summary: Follow a user
      security:
        - cookieAuth: []
      parameters:
        - in: path
          name: userId
          schema:
            type: string
          required: true
        - in: path
          name: followingId
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Unfollow user successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
                example:
                  message: 'Unfollow user successfully'
                  status: 'success'
                  result: 'success'
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UnAuthorizedError'
                example:
                  message: 'Authorization token is required | Invalid token | <error message>'
                  status: 'error'
                  statusCode: 401
        '403':
          description: Forbidden
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ForbiddenError'
                example:
                  message: 'Have no authorized to unfollow user.'
                  status: 'error'
                  statusCode: 403
        '404':
          description: NotFound
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/NotFoundError'
                example:
                  message: 'Follow relation does not exist'
                  status: 'error'
                  statusCode: 404
        '500':
          description: InternalServerError
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InternalServerError'
                example:
                  message: '<error message>'
                  status: 'error'
                  statusCode: 500
