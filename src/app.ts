import express, { Express } from 'express';
import logger from './global/utils/winston';
import { config } from './config';
import { connectDatabase } from './setupDatabase';
import { MusicshareServer } from './setupServer';

class Application {
	public initialize(): void {
		this.loadConfig();
		this.connectDatabase();
		this.startServer();
		this.handleExit();
	}	

	private loadConfig(): void {
		config.validateConfig();
	}

	private connectDatabase(): void {
		connectDatabase();
	}

	private startServer(): void {
		const app: Express = express();
		const server: MusicshareServer = new MusicshareServer(app);
		server.start();
	}

	private handleExit(): void {
		process.on('SIGINT', () => {
			process.exit(0);
		});

		process.on('SIGTERM', () => {
			process.exit(2);
		});

		process.on('exit', () => {
			logger.info('Exiting');
		});
	}
}

const appliaction: Application = new Application();
appliaction.initialize();
