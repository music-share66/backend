import { Application } from 'express';
import { healthRoutes } from './healthRoutes';
import { authRoute } from './features/auth/route/auth.route';
import { songRoute } from './features/song/route/song.route';
import { userRoute } from './features/user/route/user.route';

const BASE_PATH: string = '/api/v1';

export default (app: Application): void => {
	const routes = () => {
		app.use('', healthRoutes.health());
		app.use('', healthRoutes.env());

		app.use(BASE_PATH, authRoute.routes());
		app.use(BASE_PATH, songRoute.routes());
		app.use(BASE_PATH, userRoute.routes());
	};

	routes();
};