import { Router } from 'express';
import { SchemaValidateMiddleware } from '../../../global/middleware/schemaValidateMiddleware';
import { AuthMiddleware } from '../../../global/middleware/authMiddleware';
import signupBody from '../schema/signupBody.schema';
import signinBody from '../schema/signinBody.schema';
import { authController } from '../controller/auth.controller';

class AuthRoute {
	private router: Router;

	constructor() {
		this.router = Router();
	}

	public routes(): Router {
		this.router.get('/signin/user', AuthMiddleware.verifyUser(), authController.getSigninUser);
		this.router.get('/google/signin', authController.googleSignin);
		this.router.get('/google/callback', authController.googleCallback);

		this.router.post('/signup', SchemaValidateMiddleware.validateBody(signupBody), authController.signup);
		this.router.post('/signin', SchemaValidateMiddleware.validateBody(signinBody), authController.signin);
		this.router.post('/signout', AuthMiddleware.verifyUser(), authController.signout);

		return this.router;
	}
}

export const authRoute = new AuthRoute();
