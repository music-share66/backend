import { Signup } from '../services/signup';
import { User } from '../../../entities/user.entity';
import { UserRepository } from '../../../global/service/db/user.db';
import { UserFactory } from '../../user/services/userFactory';

interface ICondition {
  [key: string]: string | number | boolean;
}

interface UserData {
  email: string;
  passwordHash?: string;
  userName: string;
  idToken?: string;
  refreshToken?: string;
}

describe('Signup', () => {
	let signup: Signup;

	const mockUserRepository = {
		findOneBy: async (condition: ICondition) => {
			if (condition.email === 'exist@example.com') {
				const user = new User();
				return user;
			} else {
				return null;
			}
		},
		create: async (user: User) => {
			return user;
		}
	};

	const mockUserFactory = {
		// eslint-disable-next-line @typescript-eslint/no-unused-vars
		createUser: async (_userType: string, userData: UserData) => {
			return {
				id: 'xxxx-xxxx-xxxx-xxxx',
				email: userData.email,
				passwordHash: userData.passwordHash,
				userName: userData.userName,
			};
		}
	};

	beforeEach(() => {
		signup = new Signup(mockUserRepository as unknown as UserRepository, mockUserFactory as unknown as UserFactory);
	});

	test('happy case : should create a new user if the email does not exist', async () => {
		// act
		const result = signup.register('test@example.com', 'password', 'username');

		// assert
		await expect(result).resolves.toEqual('username');
	});

	test('bad case : should throw an error if the email already exists', async () => {
		// act
		const result = signup.register('exist@example.com', 'password', 'username');

		// assert
		await expect(result).rejects.toThrow('Email already exist');
	});
});
