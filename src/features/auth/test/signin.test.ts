import axios from 'axios';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { UnAuthorizedError } from '../../../global/utils/customError';
import { LocalSignin, GoogleSignin } from '../services/signin';
import { User } from '../../../entities/user.entity';
import { UserRepository } from '../../../global/service/db/user.db';
import { UserFactory } from '../../user/services/userFactory';

// Mock bcrypt
jest.mock('bcrypt', () => ({
	compare: jest.fn(),
}));

// Mock jwt
jest.mock('jsonwebtoken', () => ({
	sign: jest.fn(),
}));

// Mock axios
jest.mock('axios', () => ({
	post: jest.fn(),
	get: jest.fn(),
}));

// Mock Date
jest.spyOn(global, 'Date').mockImplementation(() => new Date(2024, 5, 1));

// Mock UserRepository
const mockUserRepository = {
	findOneBy: jest.fn(),
	create: jest.fn(),
};

// Mock UserFactory
const mockUserFactory = {
	createUser: jest.fn(),
};
	
describe('LocalSignin', () => {
	let localSignin: LocalSignin;

	beforeEach(() => {
		jest.resetAllMocks();
		localSignin = new LocalSignin(mockUserRepository as unknown as UserRepository);
	});

	test('happy case: should return JWT token if login is successful', async () => {
		// Arrange
		const user = new User();
		user.id = 'xxxx-xxxx-xxxx-xxxx';
		user.userName = 'testUser';
		user.passwordHash = 'hashedPassword';
		mockUserRepository.findOneBy.mockResolvedValue(user);
		(bcrypt.compare as jest.Mock).mockResolvedValue(true);
		(jwt.sign as jest.Mock).mockReturnValue('jwtToken');

		// Act
		const result = await localSignin.login('test@example.com', 'password');

		// Assert
		expect(result).toBe('jwtToken');
		expect(jwt.sign).toHaveBeenCalledWith(
			{ authType: 'local', time: Date(), userId: user.id, userName: user.userName },
			expect.any(String), // Assuming JWT_SECRET_KEY is mocked elsewhere
			{ expiresIn: '1h' }
		);
	});

	test('bad case: should throw UnAuthorizedError if password does not match', async () => {
		// Arrange
		const user = new User();
		user.id = 'xxxx-xxxx-xxxx-xxxx';
		user.userName = 'testUser';
		user.passwordHash = 'hashedPassword';
		mockUserRepository.findOneBy.mockResolvedValue(user);
		(bcrypt.compare as jest.Mock).mockResolvedValue(false);

		// Act & Assert
		expect(localSignin.login('test@example.com', 'wrongPassword')).rejects.toThrow(UnAuthorizedError);
	});

	test('bad case: should throw UnAuthorizedError if passwordHash is null', async () => {
		// Arrange
		const user = new User();
		user.id = 'xxxx-xxxx-xxxx-xxxx';
		user.userName = 'testUser';
		user.passwordHash = null;
		mockUserRepository.findOneBy.mockResolvedValue(user);

		// Act & Assert
		expect(localSignin.login('test@example.com', 'wrongPassword')).rejects.toThrow(UnAuthorizedError);
	});

	test('bad case: should throw UnAuthorizedError if user not found', async () => {
		// Arrange
		mockUserRepository.findOneBy.mockResolvedValue(null);

		// Act & Assert
		expect(localSignin.login('test@example.com', 'password')).rejects.toThrow(UnAuthorizedError);
	});
});

describe('GoogleSignin', () => {
	let googleSignin: GoogleSignin;

	beforeEach(() => {
		jest.resetAllMocks();
		googleSignin = new GoogleSignin(
			mockUserRepository as unknown as UserRepository,
			mockUserFactory as unknown as UserFactory
		);
	});

	test('happy case: should return Google OAuth consent screen URL', () => {
		// Act
		const result = googleSignin.getGoogleOauthConsentScreenUrl();

		// Assert
		expect(result).toMatch(/https:\/\/accounts\.google\.com\/o\/oauth2\/v2\/auth/);
	});

	test('happy case: should return JWT token if login is successful', async () => {
		// Arrange
		const mockAccessTokenDataResponse = {
			data: {
				id_token: 'mockIdToken',
				refresh_token: 'mockRefreshToken',
			},
		};
		(axios.post as jest.Mock).mockResolvedValue(mockAccessTokenDataResponse);

		const mockGoogleUserDataResponse = {
			data: { 
				mail: 'xxx@gmail.com',
				name: 'testUser'
			}
		};
		(axios.get as jest.Mock).mockResolvedValue(mockGoogleUserDataResponse);

		const user = new User();
		user.id = 'xxxx-xxxx-xxxx-xxxx';
		user.userName = 'testUser';
		user.idToken = '111';
		user.refreshToken = '222';
		mockUserRepository.findOneBy.mockResolvedValue(user);

		user.idToken = 'xxx';
		user.refreshToken = 'xxx';
		mockUserRepository.create.mockResolvedValue(user);

		(jwt.sign as jest.Mock).mockReturnValue('jwtToken');

		// Act
		const result = await googleSignin.login('googleToken');

		// Assert
		expect(result).toBe('jwtToken');
		expect(jwt.sign).toHaveBeenCalledWith(
			{ authType: 'google', time: Date(), userId: user.id, userName: user.userName },
			expect.any(String), // Assuming JWT_SECRET_KEY is mocked elsewhere
			{ expiresIn: '1h' }
		);
	});

	test('happy case:should return JWT token if login is successful and user didnt exist', async () => {
		const mockAccessTokenDataResponse = {
			data: {
				id_token: 'mockIdToken',
				refresh_token: 'mockRefreshToken',
			},
		};
		(axios.post as jest.Mock).mockResolvedValue(mockAccessTokenDataResponse);

		const mockGoogleUserDataResponse = {
			data: { 
				mail: 'xxx@gmail.com',
				name: 'testUser'
			}
		};
		(axios.get as jest.Mock).mockResolvedValue(mockGoogleUserDataResponse);

		mockUserRepository.findOneBy.mockResolvedValue(null);



		const user = new User();
		user.email = 'xxx@gmail.com';
		user.userName = 'testUser';
		user.idToken = 'xxx';
		user.refreshToken = 'xxx';
		mockUserFactory.createUser.mockResolvedValue(user);

		user.id = 'xxxx-xxxx-xxxx-xxxx';
		mockUserRepository.create.mockResolvedValue(user);

		(jwt.sign as jest.Mock).mockReturnValue('jwtToken');

		// Act
		const result = await googleSignin.login('googleToken');

		// Assert
		expect(result).toBe('jwtToken');
		expect(jwt.sign).toHaveBeenCalledWith(
			{ authType: 'google', time: Date(), userId: user.id, userName: user.userName },
			expect.any(String), // Assuming JWT_SECRET_KEY is mocked elsewhere
			{ expiresIn: '1h' }
		);
	});

	test('bad case: should throw UnAuthorizedError if call googleAccessTokenUrl faile', async () => {
		// Arrange
		(axios.post as jest.Mock).mockRejectedValue(new Error('mock error'));

		// Act & Assert
		expect(googleSignin.login('googleToken')).rejects.toThrow(UnAuthorizedError);
	});

	test('bad case: should throw UnAuthorizedError if call googleUserDataUrl faile', async () => {
		// Arrange
		// Arrange
		const mockAccessTokenDataResponse = {
			data: {
				id_token: 'mockIdToken',
				refresh_token: 'mockRefreshToken',
			},
		};
		(axios.post as jest.Mock).mockResolvedValue(mockAccessTokenDataResponse);

		(axios.get as jest.Mock).mockRejectedValue(new Error('mock error'));

		// Act & Assert
		expect(googleSignin.login('googleToken')).rejects.toThrow(UnAuthorizedError);
	});
});
