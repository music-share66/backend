import { UserRepository } from '../../../global/service/db/user.db';
import { Auth } from '../services/auth';
import { IUser } from '../interfaces/auth.interfaces';

const mockUserRepository = {
	findOne: jest.fn(),
};

describe('Auth', () => {
	let auth: Auth;

	beforeEach(() => {
		auth = new Auth(
			mockUserRepository as unknown as UserRepository,
		);
	});

	test('should return the same user object when getSigninUser is called', async () => {
		mockUserRepository.findOne.mockResolvedValue({
			id: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'Test User',
			following: [],
		});

		const user: IUser = {
			userId: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'Test User',
		};

		const signinUser = await auth.getSigninUser(user);

		expect(signinUser).toEqual({
			userId: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'Test User',
			followingUser: [],
		});
	});
});
