import { Request, Response, NextFunction } from 'express';
import HTTP_STATUS from 'http-status-codes';
import { config } from '../../../config';
import { auth } from '../services/auth';
import { signup } from '../services/signup';
import { localSignin, googleSignin } from '../services/signin';

class AuthController {
	public async getSigninUser(req: Request, res: Response, next: NextFunction) {
		try {
			const { user } = req;
			const signinUser = await auth.getSigninUser(user);
			res.status(HTTP_STATUS.OK).json({ status: 'success', message: 'User data retrieved successfully', result: { signinUser }});
		} catch (error) {
			next(error);
		}
	}

	public async signup(req: Request, res: Response, next: NextFunction) {
		try {
			const { email, password, userName } = req.body;
			const result = await signup.register( email, password, userName);
			res.status(HTTP_STATUS.CREATED).json({ status: 'success', message: 'User created successfully', result });
		} catch (error) {
			next(error);
		}
	}

	public async signin(req: Request, res: Response, next: NextFunction) {
		try {
			const { email, password } = req.body;
			const jwtToken = await localSignin.login(email, password);
			res.status(HTTP_STATUS.OK)
				.cookie('Authorization', `Bearer ${jwtToken}`, { httpOnly: true, maxAge: 3600000 })
				.json({ status: 'success', message: 'User logged in successfully', result: 'success' });
		} catch (error) {
			next(error);
		}
	}

	public async signout(_req: Request, res: Response, next: NextFunction) {
		try {
			res.clearCookie('Authorization');
			res.status(HTTP_STATUS.OK).clearCookie('Authorization').json({ status: 'success', message: 'User logged out successfully', result: 'success' });
		} catch (error) {
			next(error);
		}
	}

	public googleSignin(_req: Request, res: Response, next: NextFunction) {
		try {
			const googleOauthConsentScreenUrl = googleSignin.getGoogleOauthConsentScreenUrl();
			res.status(HTTP_STATUS.OK)
				.json({ status: 'success', message: 'Get redirectUrl successfully', result: { googleOauthConsentScreenUrl } });
		} catch (error) {
			next(error);
		}
	}

	public async googleCallback(req: Request, res: Response, next: NextFunction) {
		try {
			const { code } = req.query;
			const jwtToken = await googleSignin.login(code as string);
			res.status(HTTP_STATUS.MOVED_PERMANENTLY)
				.cookie('Authorization', `Bearer ${jwtToken}`, { httpOnly: true, maxAge: 3600000 })
				.redirect(config.CLIENT_HOST as string);
		} catch (error) {
			next(error);
		}
	}
}

export const authController = new AuthController();
