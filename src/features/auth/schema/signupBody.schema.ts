import Joi from 'joi'; 

const signupBody = Joi.object({
	email: Joi.string().email().required(),
	password: Joi.string().min(8).required(),
	userName: Joi.string().required(),
	repeatPassword: Joi.ref('password')
});

export default signupBody;