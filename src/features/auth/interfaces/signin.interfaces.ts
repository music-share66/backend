export interface IJwtData {
  time: string;
  userId: string;
  userName: string;
  authType: string;
}
