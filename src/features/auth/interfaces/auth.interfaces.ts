export interface IUser {
  userId: string,
  userName: string,
  followingUser?: IFollowingUser[]
}

export interface IFollowingUser {
  id: string;
  followee: {
    id: string;
    userName: string;
  };
}
