import bcrypt from 'bcrypt';
import { ConflictError } from '../../../global/utils/customError';
import { UserFactory, userFactory } from '../../user/services/userFactory';
import { User } from '../../../entities/user.entity';
import { UserRepository, userRepository } from '../../../global/service/db/user.db';

export class Signup {
	private userRepository: UserRepository;
	private userFactory: UserFactory;

	constructor(userRepository: UserRepository, userFactory: UserFactory) {
		this.userRepository = userRepository;
		this.userFactory = userFactory;
	}

	public async register(email: string, password: string, userName: string): Promise<string>{
		const isEmailExist = await this.getIsEmailExist(email);
		if (isEmailExist) { throw new ConflictError('Email already exist'); }
		const passwordHash = this.getPasswordHash(password);
		const newUser = await this.createUser(email, passwordHash, userName);
		return newUser.userName;
	}

	private async getIsEmailExist(email: string): Promise<boolean> {
		const user = await this.userRepository.findOneBy({ email });
		return user ? true : false;
	}

	private getPasswordHash(password: string): string {
		return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
	}

	private async createUser(email: string, passwordHash: string, userName: string): Promise<User> {
		const userType = 'local';
		const userData = {
			email,
			passwordHash,
			userName,
		};
		const user = this.userFactory.createUser(userType, userData);
		const newUser = await this.userRepository.create(user);

		return newUser;
	}
}

export const signup = new Signup(userRepository, userFactory);
