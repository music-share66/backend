import { UserRepository, userRepository } from '../../../global/service/db/user.db';
import { IUser, IFollowingUser } from '../interfaces/auth.interfaces';


export class Auth {
	private userRepository: UserRepository;

	constructor(userRepository: UserRepository) {
		this.userRepository = userRepository;
	}

	public async getSigninUser(user: IUser): Promise<IUser> {
		const { userId } = user;
		const followingUser = await this.getFollowingUser(userId);

		const userData = {
			...user,
			followingUser,
		};

		return userData;
	}

	private async getFollowingUser(userId: string): Promise<IFollowingUser[]>{
		const followingUser = await this.userRepository.findOne({
			select: {
				id: true,
				following: {
					id: true,
					createDate: true,
					followee: {
						id: true,
						userName: true,
					}
				}
			},
			where: { id: userId },
			relations: ['following', 'following.followee'],
			order: {
				following: {
					createDate: 'DESC',
				}
			},
		});
		const { following } = Array.isArray(followingUser) ? followingUser[0] : followingUser || [];

		return following;
	}
}

export const auth = new Auth(userRepository);
