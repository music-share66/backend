import axios from 'axios';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { config } from '../../../config';
import { UnAuthorizedError } from '../../../global/utils/customError';
import { IJwtData } from '../interfaces/signin.interfaces';
import { UserFactory, userFactory } from '../../user/services/userFactory';
import { User } from '../../../entities/user.entity';
import { UserRepository, userRepository } from '../../../global/service/db/user.db';

export class Signin {
	protected userRepository: UserRepository;

	constructor(userRepository: UserRepository) {
		this.userRepository = userRepository;
	}

	protected async getUserByEmail(email: string): Promise<User | null> {
		return await this.userRepository.findOneBy({ email });
	}

	protected getJwtToken(jwtData: IJwtData): string {
		return jwt.sign(jwtData, config.JWT_SECRET_KEY as string, { expiresIn: '1h' });
	}
}

export class LocalSignin extends Signin {
	constructor(userRepository: UserRepository) {
		super(userRepository);
	}

	public async login(email: string, password: string): Promise<string> {
		const user = await this.getUserByEmail(email);
		if (!user) { throw new UnAuthorizedError('Invalid email or password'); }
		const { id, userName, passwordHash } = user;

		if (!passwordHash) { throw new UnAuthorizedError('Invalid email or password'); }
		const isPasswordMatch = await this.getIsPasswordMatch(password, passwordHash);
		if (!isPasswordMatch) { throw new UnAuthorizedError('Invalid email or password'); }

		const jwtData = {
			userName,
			time: Date(),
			userId: id,
			authType: 'local',
		};
		const jwtToken = this.getJwtToken(jwtData);

		return jwtToken;
	}

	private async getIsPasswordMatch(password: string, passwordHash: string): Promise<boolean> {
		return await bcrypt.compare(password, passwordHash);
	}
}

export class GoogleSignin extends Signin {
	private userFactory: UserFactory;

	constructor(userRepository: UserRepository, userFactory: UserFactory) {
		super(userRepository);
		this.userFactory = userFactory;
	}

	public getGoogleOauthConsentScreenUrl(): string {
		const googleOauthUrl = config.GOOGLE_OAUTH_URL;
		const googleClientId = config.GOOGLE_CLIENT_ID;
		const googleCallBackUrl = config.GOOGLE_CALL_BACK_URL;
		const googleOauthScpoes = [
			'https%3A//www.googleapis.com/auth/userinfo.email',
			'https%3A//www.googleapis.com/auth/userinfo.profile',
		];
		const state = 'some_state';
		const scopes = googleOauthScpoes.join(' ');
		const googleOauthConsentScreenUrl = `${googleOauthUrl}?client_id=${googleClientId}&redirect_uri=${googleCallBackUrl}&access_type=offline&response_type=code&state=${state}&scope=${scopes}`;

		return googleOauthConsentScreenUrl;
	}

	public async login(code: string): Promise<string> {
		const { googleUserData, idToken, refreshToken } = await this.getGoogleUserData(code);
		const { email, name: userName } = googleUserData;

		const updatedUser = await this.getUpdatedUser(email, userName, idToken, refreshToken);
		const jwtData = {
			userName,
			time: Date(),
			userId: updatedUser.id,
			authType: 'google',
		};
		const jwtToken = this.getJwtToken(jwtData);

		return jwtToken;
	}

	private async getGoogleUserData(code: string): Promise<{ googleUserData: {[key: string]: string}; idToken: string; refreshToken: string }>{
		const { idToken, refreshToken } = await this.getTokenData(code);
		const googleUserDataUrl = `${config.GOOGLE_TOKEN_INFO_URL}?id_token=${idToken}`;
		const googleUserData = await axios.get(`${googleUserDataUrl}`)
			.then((res) => res.data)
			.catch((err) => {
				throw new UnAuthorizedError(err.message);
			});

		return { googleUserData, idToken, refreshToken };
	}

	private async getTokenData(code: string): Promise<{ idToken: string; refreshToken: string }>{
		const data = {
			code,
			client_id: config.GOOGLE_CLIENT_ID,
			client_secret: config.GOOGLE_CLIENT_SECRET,
			redirect_uri: config.GOOGLE_CALL_BACK_URL,
			grant_type: 'authorization_code',
			access_type: 'offline',
		};

		// exchange authorization code for id_token and refresh_token
		const googleAccessTokenUrl = config.GOOGLE_ACCESS_TOKEN_URL;
		const accessTokenData = await axios.post(`${googleAccessTokenUrl}`, data)
			.then((res) => res.data)
			.catch((err) => {
				throw new UnAuthorizedError(err.message);
			});
		const { id_token: idToken, refresh_token: refreshToken } = accessTokenData;

		return { idToken, refreshToken };
	}

	private async getUpdatedUser(email: string, userName: string, idToken: string, refreshToken: string): Promise<User> {
		let googleUser = await this.getUserByEmail(email);
		if (googleUser) {
			// user exists, update user token refresh token
			googleUser.passwordHash = null;
			googleUser.idToken = idToken;
			googleUser.refreshToken = refreshToken;
		} else {
			// user not exit, create user
			const userType = 'google';
			const userData = {
				email,
				userName,
				idToken,
				refreshToken,
			};
			googleUser = this.userFactory.createUser(userType, userData);
		}
		const updatedUser = await this.userRepository.create(googleUser as User);

		return updatedUser;
	}
}

export const localSignin = new LocalSignin(userRepository);
export const googleSignin = new GoogleSignin(userRepository, userFactory);
