import { Request, Response, NextFunction } from 'express';
import HTTP_STATUS from 'http-status-codes';
import { userFinder } from '../services/userFinder';
import { userSongs } from '../services/userSongs';
import { followUser } from '../services/followUser';
import { unfollowUser } from '../services/unfollowUser';

class UserController {
	public async getUsersByName(req: Request, res: Response, next: NextFunction) {
		try {
			const { params } = req;
			const { userName } = params;
			const user = await userFinder.getUsersByName(userName);
			res.status(HTTP_STATUS.OK).json({ status: 'success', message: 'Get user by name successfully', result: user });
		} catch (error) {
			next(error);
		}
	}

	public async getUserSongs(req: Request, res: Response, next: NextFunction) {
		try {
			const { params } = req;
			const { userId } = params;
			const songs = await userSongs.getUserSongs(userId);
			res.status(HTTP_STATUS.OK).json({ status: 'success', message: 'Get user songs successfully', result: songs });
		} catch (error) {
			next(error);
		}
	}

	public async followUser(req: Request, res: Response, next: NextFunction) {
		try {
			const { user: userData, params, body } = req;
			const { userId } = params;
			const { followeeId } = body;
			const following = await followUser.follow(userData, userId, followeeId);
			res.status(HTTP_STATUS.OK).json({ status: 'success', message: 'Follow user successfully', result: following });
		} catch (error) {
			next(error);
		}
	}

	public async unfollowUser(req: Request, res: Response, next: NextFunction) {
		try {
			const { user: userData, params } = req;
			const { userId, followingId } = params;
			await unfollowUser.unfollow(userData, userId, followingId);
			res.status(HTTP_STATUS.OK).json({ status: 'success', message: 'Unfollow user successfully', result: 'Success' });
		} catch (error) {
			next(error);
		}
	}
}

export const userController = new UserController();
