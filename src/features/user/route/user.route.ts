import { Router } from 'express';
import { AuthMiddleware } from '../../../global/middleware/authMiddleware';
import { SchemaValidateMiddleware } from '../../../global/middleware/schemaValidateMiddleware';
import followUserBody from '../schema/followUser.schema';
import { userController } from '../controllers/user.controller';

class UserRoute {
	private router: Router;

	constructor() {
		this.router = Router();
	}

	public routes(): Router {
		this.router.get('/users/name/:userName', userController.getUsersByName);
		this.router.get('/users/:userId/songs', userController.getUserSongs);

		this.router.post(
			'/users/:userId/following',
			AuthMiddleware.verifyUser(),
			SchemaValidateMiddleware.validateBody(followUserBody),
			userController.followUser
		);

		this.router.delete(
			'/users/:userId/following/:followingId',
			AuthMiddleware.verifyUser(),
			userController.unfollowUser
		);

		return this.router;
	}
}

export const userRoute = new UserRoute();
