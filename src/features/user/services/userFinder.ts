import { Like } from 'typeorm';
import { UserRepository, userRepository } from '../../../global/service/db/user.db';

export class UserFinder {
	private userRepository: UserRepository;

	constructor(userRepository: UserRepository) {
		this.userRepository = userRepository;
	}

	public async getUsersByName(name: string) {
		const users = await this.userRepository.find({
			select: {
				id: true,
				userName: true,
			},
			where: {
				userName: Like(`%${name}%`),
			},
			order: {
				userName: 'ASC',
			},
		});

		return users;
	}
}

export const userFinder = new UserFinder(userRepository);
