import { ForbiddenError, NotFoundError } from '../../../global/utils/customError';
import { User } from '../../../entities/user.entity';
import { FollowUser as FollowUserEntity } from '../../../entities/followUser.entity';
import { IUser } from '../../auth/interfaces/auth.interfaces';
import { INewFollowUser } from '../interfaces/followUser.interfaces';
import { FollowUserFactory, followUserFactory } from './followUserFactory';
import { UserRepository, userRepository } from '../../../global/service/db/user.db';
import { FollowUserRepository, followUserRepository } from '../../../global/service/db/followUser.db';

export class FollowUser {
	private userRepository: UserRepository;
	private followUserRepository: FollowUserRepository;
	private followUserFactory: FollowUserFactory;

	constructor(userRepository: UserRepository, followUserRepository: FollowUserRepository, followUserFactory: FollowUserFactory) {
		this.userRepository = userRepository;
		this.followUserRepository = followUserRepository;
		this.followUserFactory = followUserFactory;
	}

	public async follow(userData: IUser, userId: string, followeeId: string): Promise<INewFollowUser> {
		const isAuthorizedToFollow = this.getIsAuthorizedToFollow(userData, userId);
		if (!isAuthorizedToFollow) { throw new ForbiddenError('Have no authorized to follow user'); }

		const follower = await this.getUserById(userId);
		const followee = await this.getUserById(followeeId);
		if (!follower || !followee) { throw new NotFoundError('User does not exist'); }

		const newFollowUser = await this.createFollowUser(follower, followee);

		const followUser = {
			id: newFollowUser.id,
			createDate: newFollowUser.createDate,
			followee: {
				id: newFollowUser.followee.id,
				userName: newFollowUser.followee.userName,
			},
		};

		return followUser;
	}

	private getIsAuthorizedToFollow(userData: IUser, userId: string): boolean {
		const { userId: requestUserId } = userData;

		return requestUserId === userId;
	}

	private async getUserById(userId: string): Promise<User | null> {
		const user = await this.userRepository.findOneBy({ id: userId });

		return user;
	}

	private async createFollowUser(follower: User, followee: User): Promise<FollowUserEntity> {
		const followUser = this.followUserFactory.createFollowUser(follower, followee);
		const newFollowUser = this.followUserRepository.create(followUser);

		return newFollowUser;
	}
}

export const followUser = new FollowUser(userRepository, followUserRepository, followUserFactory);
