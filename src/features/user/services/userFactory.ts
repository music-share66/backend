import { InternalServerError } from '../../../global/utils/customError';
import  { UserData, LocalUserData, GoogleUserData } from '../interfaces/userFactory.interfaces';
import { User } from '../../../entities/user.entity';

export class UserFactory {
	public createUser(userType: string, userData: UserData): User {
		switch (userType) {
			case 'local':
				return this.createLocalUser(userData as LocalUserData);

			case 'google':
				return this.createGoogleUser(userData as GoogleUserData);

			default:
				throw new InternalServerError('Invalid user type');
		}
	}

	private createLocalUser(localUserData: LocalUserData): User {
		const { email, passwordHash, userName } = localUserData;
		const localUser = new User();
		localUser.email = email;
		localUser.passwordHash = passwordHash;
		localUser.userName = userName;

		return localUser;
	}

	private createGoogleUser(googleUserData: GoogleUserData): User {
		const { email, userName, idToken, refreshToken } = googleUserData;
		const googleUser = new User();
		googleUser.email = email;
		googleUser.userName = userName;
		googleUser.idToken = idToken;
		googleUser.refreshToken = refreshToken;		

		return googleUser;
	}
}

export const userFactory = new UserFactory();
