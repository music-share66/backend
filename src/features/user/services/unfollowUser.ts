import { ForbiddenError, NotFoundError } from '../../../global/utils/customError';
import { FollowUser } from '../../../entities/followUser.entity';
import { IUser } from '../../auth/interfaces/auth.interfaces';
import { FollowUserRepository, followUserRepository } from '../../../global/service/db/followUser.db';

export class UnfollowUser {
	private followUserRepository: FollowUserRepository;

	constructor(followUserRepository: FollowUserRepository) {
		this.followUserRepository = followUserRepository;
	}

	public async unfollow(userData: IUser, userId: string, followingId: string): Promise<void> {
		const isAuthorizedToUnfollow = this.getIsAuthorizedToUnfollow(userData, userId);
		if (!isAuthorizedToUnfollow) { throw new ForbiddenError('Have no authorized to unfollow user.'); }

		const followUser = await this.getFollowUserById(followingId);
		if (!followUser) { throw new NotFoundError('Follow relation does not exist'); }

		const isFollower = this.getIsFollower(followUser, userId);
		if (!isFollower) { throw new ForbiddenError('Have no authorized to unfollow user.'); }

		await this.deleteFollowUser(followUser);
	}

	private async getFollowUserById(followingId: string): Promise<FollowUser | null> {
		const followUser = await this.followUserRepository.findOne({
			select: {
				id: true,
				follower: {
					id: true,
				}
			},
			where: {
				id: followingId
			},
			relations: ['follower']
		});

		return followUser;
	}

	private getIsAuthorizedToUnfollow(userData: IUser, userId: string): boolean {
		const { userId: requestUserId } = userData;

		return requestUserId === userId;
	}

	private getIsFollower(followUser: FollowUser, userId: string): boolean {
		const { follower } = followUser;
		const { id: followerId } = follower;

		return followerId === userId;
	}

	private async deleteFollowUser(followUser: FollowUser): Promise<void> {
		await this.followUserRepository.delete(followUser);
	}  
}

export const unfollowUser = new UnfollowUser(followUserRepository);
