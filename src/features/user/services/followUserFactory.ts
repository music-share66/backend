import { FollowUser } from '../../../entities/followUser.entity';
import { User } from '../../../entities/user.entity';

export class FollowUserFactory {
	public createFollowUser(follower: User, followee: User): FollowUser {
		const followUser = new FollowUser();
		followUser.follower = follower;
		followUser.followee = followee;

		return followUser;
	}
}

export const followUserFactory = new FollowUserFactory();
