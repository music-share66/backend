import { IUserSongs } from '../interfaces/userSongs.interfaces';
import { UserCache, userCache } from '../../../global/cache/user.cache';
import { UserRepository, userRepository } from '../../../global/service/db/user.db';

export class UserSongs {
	private userCache: UserCache;
	private userRepository: UserRepository;

	constructor(userCache: UserCache, userRepository: UserRepository) {
		this.userCache = userCache;
		this.userRepository = userRepository;
	}

	public async getUserSongs(userId: string): Promise<IUserSongs | null> {
		const cacheSongs = await this.userCache.getUserSongs(userId);
		if (cacheSongs) { return cacheSongs; }

		const userSongs = await this.userRepository.findOne({
			select: {
				id: true,
				userName: true,
				songs: {
					id: true,
					songName: true,
					durationInMs: true,
					createDate: true,
					processingStatus: true,
				}
			},
			where: { id: userId },
			order: {
				songs: {
					createDate: 'ASC'
				}
			},
			relations: ['songs'],
		});

		if (userSongs?.songs) { 
			await this.userCache.setUserSongs(userId, userSongs);
		}

		return userSongs;
	}
}

export const userSongs = new UserSongs(userCache, userRepository);
