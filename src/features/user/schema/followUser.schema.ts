import Joi from 'joi'; 

const followUserBody = Joi.object({
	followeeId: Joi.string().uuid().required(),
});

export default followUserBody;
