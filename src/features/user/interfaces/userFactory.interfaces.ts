export interface UserData {
  email: string;
  passwordHash?: string;
  userName: string;
  idToken?: string;
  refreshToken?: string;
}

export interface LocalUserData {
  email: string;
  passwordHash: string;
  userName: string;
}

export interface GoogleUserData {
  email: string;
  userName: string;
  idToken: string;
  refreshToken: string;
}