export interface INewFollowUser {
  id: string;
  createDate: Date;
  followee: {
    id: string;
    userName: string;
  };
}
