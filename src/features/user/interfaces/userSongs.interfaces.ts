import { Song } from '../../../entities/song.entity';

export interface IUserSongs {
  id: string;
  userName: string;
  songs: Song[];
}
