import { UserSongs } from '../services/userSongs';
import { UserCache } from '../../../global/cache/user.cache';
import { UserRepository } from '../../../global/service/db/user.db';

const mockUserCache = {
	getUserSongs: jest.fn(),
	setUserSongs: jest.fn()
};

const mockUserRepository = {
	findOne: jest.fn()
};

describe('UserSongs', () => {
	let userSongs: UserSongs;
	beforeEach(() => {
		jest.resetAllMocks();
		userSongs = new UserSongs(
			mockUserCache as unknown as UserCache,
      mockUserRepository as unknown as UserRepository
		);
	});

	test('happy case : should return user songs from cache', async () => {
		// Arrange
		mockUserCache.getUserSongs.mockResolvedValue({
			id: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'user1',
			songs: [
				{
					id: 'yyyy-yyyy-yyyy-yyyy',
					songName: 'song1',
					durationInMs: 10000,
					processingStatus: 'COMPLETE',
					createDate: '2024-05-04T16:14:23.493Z',
				}
			]
		});

		// Act
		const result = await userSongs.getUserSongs('xxxx-xxxx-xxxx-xxxx');

		// Assert
		expect(result).toMatchObject({
			id: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'user1',
			songs: [
				{
					id: 'yyyy-yyyy-yyyy-yyyy',
					songName: 'song1',
					durationInMs: 10000,
					processingStatus: 'COMPLETE',
					createDate: '2024-05-04T16:14:23.493Z',
				}
			]
		});
	});

	test('happy case : should return user id, userName, songs[] and store userSongs to cache if cache not found', async () => {
		mockUserRepository.findOne.mockResolvedValue({
			id: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'user1',
			songs: [
				{
					id: 'yyyy-yyyy-yyyy-yyyy',
					songName: 'song1',
					durationInMs: 10000,
					processingStatus: 'COMPLETE',
					createDate: '2024-05-04T16:14:23.493Z',
				}
			]
		});

		const result = await userSongs.getUserSongs('xxxx-xxxx-xxxx-xxxx');

		expect(result).toMatchObject({
			id: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'user1',
			songs: [
				{
					id: 'yyyy-yyyy-yyyy-yyyy',
					songName: 'song1',
					durationInMs: 10000,
					processingStatus: 'COMPLETE',
					createDate: '2024-05-04T16:14:23.493Z',
				}
			]
		});

		expect(mockUserCache.setUserSongs).toHaveBeenCalledWith('xxxx-xxxx-xxxx-xxxx', {
			id: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'user1',
			songs: [
				{
					id: 'yyyy-yyyy-yyyy-yyyy',
					songName: 'song1',
					durationInMs: 10000,
					processingStatus: 'COMPLETE',
					createDate: '2024-05-04T16:14:23.493Z',
				}
			]
		});
	});
});
