import { FollowUser } from '../services/followUser';
import { ForbiddenError, NotFoundError } from '../../../global/utils/customError';
import { IUser } from '../../auth/interfaces/auth.interfaces';
import { FollowUserFactory } from '../services/followUserFactory';
import { UserRepository } from '../../../global/service/db/user.db';
import { FollowUserRepository } from '../../../global/service/db/followUser.db';

const mockUserRepository = {
	findOneBy: jest.fn(),
};

const mockFollowUserRepository = {
	create: jest.fn(),
};

const mockFollowUserFactory = {
	createFollowUser: jest.fn(),
};

let followUser: FollowUser;

describe('FollowUser', () => {
	beforeEach(() => {
		jest.resetAllMocks();
		followUser = new FollowUser(
      mockUserRepository as unknown as UserRepository,
      mockFollowUserRepository as unknown as  FollowUserRepository,
      mockFollowUserFactory as unknown as  FollowUserFactory
		);
	});

	test('happy case : should follow user successfully', async () => {
		// Arrange
		const mockFollower = {
			id: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'testFollower',
		};

		const mockFollowee = {
			id: 'yyyy-yyyy-yyyy-yyyy',
			userName: 'testFollowee',
		};

		const mockFollowUser = {
			id: 'zzzz-zzzz-zzzz-zzzz',
			follower: mockFollower,
			followee: mockFollowee,
			createDate: '2024-05-14T03:06:50.388Z',
		};

		mockUserRepository.findOneBy.mockResolvedValueOnce(mockFollower);
		mockUserRepository.findOneBy.mockResolvedValueOnce(mockFollowee);
		mockFollowUserFactory.createFollowUser.mockReturnValueOnce(mockFollowUser);
		mockFollowUserRepository.create.mockResolvedValueOnce(mockFollowUser);

		// Act
		const userData = { userId: 'aaaa-aaaa-aaaa-aaaa' } as IUser;
		const userId = 'aaaa-aaaa-aaaa-aaaa';
		const followeeId = 'bbbb-bbbb-bbbb-bbbb';
		const result = await followUser.follow(userData, userId, followeeId);

		// Assert
		expect(result).toEqual({
			id: mockFollowUser.id,
			createDate: mockFollowUser.createDate,
			followee: {
				id: mockFollowUser.followee.id,
				userName: mockFollowUser.followee.userName,
			},
		});
	});

	test('bad case : should throw ForbiddenError if not authorized', () => {
		// Arrange & Act
		const userData = { userId: '123' } as IUser;
		const userId = '456';
		const followeeId = '789';
		const result = followUser.follow(userData, userId, followeeId);

		// Assert
		expect(result).rejects.toThrow(ForbiddenError);
	});

	test('bad case : should throw NotFoundError if user not found', () => {
		// Arrange
		mockUserRepository.findOneBy.mockResolvedValueOnce(null);
		mockUserRepository.findOneBy.mockResolvedValueOnce(null);

		// Act
		const userData = { userId: '123' } as IUser;
		const userId = '123';
		const followeeId = '456';
		const result = followUser.follow(userData, userId, followeeId);

		// Assert
		expect(result).rejects.toThrow(NotFoundError);
	});
});
