import { UnfollowUser } from '../services/unfollowUser';
import { ForbiddenError, NotFoundError } from '../../../global/utils/customError';
import { FollowUserRepository } from '../../../global/service/db/followUser.db';

const mockFollowUserRepository = {
	findOne: jest.fn(),
	delete: jest.fn()
};

let unfollowUser: UnfollowUser;

describe('UnFollowUser', () => {
	beforeEach(() => {
		jest.resetAllMocks();
		unfollowUser = new UnfollowUser(mockFollowUserRepository as unknown as FollowUserRepository);
	});

	test('happy case : should delete followUser', async () => {
		// Arrange
		const mockFollowUser = {
			id: 'yyyy-yyyy-yyyy-yyyy',
			follower: {
				id: 'xxxx-xxxx-xxxx-xxxx',
			}
		};
		mockFollowUserRepository.findOne.mockResolvedValue(mockFollowUser);

		// Act
		const user = {
			userId: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'test',
		};
		const userId = 'xxxx-xxxx-xxxx-xxxx';
		const followingId = 'yyyy-yyyy-yyyy-yyyy';
		const result = await unfollowUser.unfollow(user, userId, followingId);

		// Assert
		expect(result).toEqual(undefined);
		expect(mockFollowUserRepository.findOne).toHaveBeenCalledTimes(1);
		expect(mockFollowUserRepository.delete).toHaveBeenCalledWith(mockFollowUser);
	});

	test('bad case : should throw ForbiddenError if not authorized', () => {
		// Arrange & Act
		const user = {
			userId: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'test',
		};
		const userId = 'yyyy-yyyy-yyyy-yyyy';
		const followingId = 'yyyy-yyyy-yyyy-yyyy';
		const result = unfollowUser.unfollow(user, userId, followingId);

		// Assert
		expect(result).rejects.toThrow(ForbiddenError);
	});

	test('bad case : should throw NotFoundError if followUser does not exist', () => {
		// Arrange
		mockFollowUserRepository.findOne.mockResolvedValue(null);

		// Act
		const user = {
			userId: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'test',
		};
		const userId = 'xxxx-xxxx-xxxx-xxxx';
		const followingId = 'yyyy-yyyy-yyyy-yyyy';
		const result = unfollowUser.unfollow(user, userId, followingId);

		// Assert
		expect(result).rejects.toThrow(NotFoundError);
	});

	test('bad case : should throw ForbiddenError if not authorized to unfollow', () => {
		// Arrange
		const mockFollowUser = {
			id: 'yyyy-yyyy-yyyy-yyyy',
			follower: {
				id: 'zzz-zzz-zzz-zzz',
			}
		};
		mockFollowUserRepository.findOne.mockResolvedValue(mockFollowUser);

		// Act
		const user = {
			userId: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'test',
		};
		const userId = 'xxxx-xxxx-xxxx-xxxx';
		const followingId = 'yyyy-yyyy-yyyy-yyyy';
		const result = unfollowUser.unfollow(user, userId, followingId);

		// Assert
		expect(result).rejects.toThrow(ForbiddenError);
	});
});