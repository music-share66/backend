import { UserFinder } from '../services/userFinder';
import { UserRepository } from '../../../global/service/db/user.db';

const mockUserRepository = {
	find: jest.fn(),
};

let userFinder: UserFinder;

describe('UserFinder', () => {
	beforeEach(() => {
		jest.resetAllMocks();
		userFinder = new UserFinder(
      mockUserRepository as unknown as UserRepository
		);
	});

	test('happy case : getUsersByName', async () => {
		mockUserRepository.find.mockResolvedValue([
			{ id: 1, userName: 'user1' },
			{ id: 2, userName: 'user2' },
		]);

		const result = await userFinder.getUsersByName('user');

		expect(result).toEqual([
			{ id: 1, userName: 'user1' },
			{ id: 2, userName: 'user2' },
		]);
	});
});
