import { Router } from 'express';
import { SchemaValidateMiddleware } from '../../../global/middleware/schemaValidateMiddleware';
import { AuthMiddleware } from '../../../global/middleware/authMiddleware';
import songMakerBody from '../schema/songMakerBody.schema';
import songMakerFile from '../schema/songMakerFile.schema';
import { songController } from '../controller/song.controller';

class SongRoute {
	private router: Router;

	constructor() {
		this.router = Router();
	}

	public routes(): Router {
		this.router.get(
			'/songs/:songId/m3u8file-url',
			songController.getSongM3u8FileUrl
		);

		this.router.post(
			'/songs',
			AuthMiddleware.verifyUser(),
			SchemaValidateMiddleware.validateBody(songMakerBody),
			SchemaValidateMiddleware.validateFile(songMakerFile),
			songController.createSong
		);

		this.router.put(
			'/songs/encode-status',
			AuthMiddleware.verifyUser(),
			songController.updateEncodeStatus
		);

		this.router.delete(
			'/songs/:songId',
			AuthMiddleware.verifyUser(),
			songController.deleteSong
		);

		return this.router;
	}
}

export const songRoute = new SongRoute();
