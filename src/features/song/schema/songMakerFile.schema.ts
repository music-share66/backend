import Joi from 'joi'; 

const songMakerFile = Joi.object({
	songFile: Joi.object({
		name: Joi.string().regex(/\.mp4$/i).required(),
		data: Joi.any().required(),
		size: Joi.number().required(),
		encoding: Joi.string().required(),
		tempFilePath: Joi.string().allow(''),
		truncated: Joi.boolean().required(),
		mimetype: Joi.string().valid('video/mp4').required(),
		md5: Joi.string().required(),
		mv: Joi.func().required()
	}).required()
});

export default songMakerFile;
