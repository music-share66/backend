import Joi from 'joi'; 

const songMakerBody = Joi.object({
	songName: Joi.string().required(),
});

export default songMakerBody;
