import { Request, Response, NextFunction } from 'express';
import { UploadedFile } from 'express-fileupload';
import HTTP_STATUS from 'http-status-codes';
import { songM3U8UrlGenerator } from '../services/songM3U8UrlGenerator';
import { songMaker } from '../services/songMaker';
import { hlsEncoderChecker } from '../services/hlsEncoderChecker';
import { songDeleter } from '../services/songDeleter';

class SongController {
	public async getSongM3u8FileUrl(req: Request, res: Response, next: NextFunction) {
		try {
			const { params } = req;
			const { songId } = params;
			const m3u8FileUrl = await songM3U8UrlGenerator.generate(songId);
			res.status(HTTP_STATUS.OK).json({ status: 'success', message: 'Get Song m3u8 file url successfully', result: m3u8FileUrl });
		} catch (error) {
			next(error);
		}
	}

	public createSong(req: Request, res: Response, next: NextFunction) {
		try {
			const { user: userData, body, files } = req;
			const { songName } = body;
			const songFile  = files?.songFile;
			songMaker.create(userData, songName, songFile as UploadedFile);
			res.status(HTTP_STATUS.ACCEPTED).json({ status: 'success', message: 'New song creating', result: 'success' });
		} catch (error) {
			next(error);
		}
	}

	public updateEncodeStatus(req: Request, res: Response, next: NextFunction) {
		try {
			const { user: userData } = req;
			hlsEncoderChecker.updateEncodeStatus(userData);
			res.status(HTTP_STATUS.ACCEPTED).json({ status: 'success', message: 'Updating encode status', result: 'processing' });
		} catch (error) {
			next(error);
		}
	}

	public deleteSong(req: Request, res: Response, next: NextFunction) {
		try {
			const { user: userData, params } = req;
			const { songId } = params;
			songDeleter.delete(userData, songId);
			res.status(HTTP_STATUS.ACCEPTED).json({ status: 'success', message: 'Song deleting', result: 'processing' });
		} catch (error) {
			next(error);
		}
	}
}

export const songController = new SongController();
