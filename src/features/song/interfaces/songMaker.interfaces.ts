import { SongStorageDetail } from '../../../entities/songStorageDetail.entity';

export interface IUploadSongResult {
  processingStatus: number;
  songStorageDetail: SongStorageDetail | null;
}

export interface IEncodeResult {
  processingStatus: number;
  encodeJobId: string | null;
}
