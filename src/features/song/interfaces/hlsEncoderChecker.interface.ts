export interface INewSongNotification {
  value: string;
}

export interface ISongOwner {
  id: string;
  songName: string;
  user: {
    id: string;
    userName: string;
  };
}
