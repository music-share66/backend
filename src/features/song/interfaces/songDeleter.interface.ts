import { User } from '../../../entities/user.entity';
import { SongStorageDetail } from '../../../entities/songStorageDetail.entity';

export interface ISongToDelete {
  id: string;
  user: User;
  songStorageDetail: SongStorageDetail;
}
