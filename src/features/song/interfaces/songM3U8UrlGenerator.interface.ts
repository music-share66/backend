import { SongStorageDetail } from '../../../entities/songStorageDetail.entity';

export interface ISong {
  id: string;
  songStorageDetail: SongStorageDetail;
}
