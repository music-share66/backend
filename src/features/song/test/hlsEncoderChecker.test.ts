import { NotificationQueue } from '../../../global/queue/notification.queue';
import { HlsEncoderChecker } from '../services/hlsEncoderChecker';
import { HlsEncoderService } from '../../../global/service/hlsEncoder/hlsEncoder';
import { SongRepository } from '../../../global/service/db/song.db';
import { UserCache } from '../../../global/cache/user.cache';

const mockHlsEncoderService = {
	getJob: jest.fn(),
};

const mockSongRepository = {
	find: jest.fn(),
	findOne: jest.fn(),
	save: jest.fn(),
};

const mockNotificationQueue = {
	produceNotificationMessage: jest.fn(),
};

const mockUserCache = {
	deleteUserSongs: jest.fn(),
};

describe('HlsEncoderChecker', () => {
	let hlsEncoderChecker: HlsEncoderChecker;

	beforeEach(() => {		
		jest.resetAllMocks();
		hlsEncoderChecker = new HlsEncoderChecker(
      mockHlsEncoderService as unknown as HlsEncoderService,
      mockSongRepository as unknown as SongRepository,
			mockNotificationQueue as unknown as NotificationQueue,
			mockUserCache as unknown as UserCache
		);
	});

	test('happy case : should return if getSongsByProcessingStatus return []', async () => {
		// Arrange
		mockSongRepository.find.mockResolvedValue([]);

		// Act
		const userData = {
			userId: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'testUser',
		};
		const result = await hlsEncoderChecker.updateEncodeStatus(userData);

		// Assert
		expect(mockSongRepository.find).toHaveBeenCalledTimes(1);
		expect(result).toBeUndefined();
	});

	test('happy case : should execute songRepository.save with encodedSongFileUrl and delete userSongs cache if hlsEncodeJob COMPLETE ', async () => {
		// Arrange
		mockSongRepository.find.mockResolvedValue([
			{
				encodeJobId: 'xxxx-xxxx-xxxx-xxxx',
			},
		]);

		mockHlsEncoderService.getJob.mockResolvedValue({
			$metadata: { httpStatusCode: 200 },
			Job: {
				Status: 'COMPLETE',
				OutputGroupDetails: [{
					OutputDetails: [{
						DurationInMs: 10000
					}]
				}]
			}
		});

		mockSongRepository.findOne.mockResolvedValue({
			id: 'zzzz-zzzz-zzzz-zzzz',
			songName: 'test song',
			user: {
				id: 'aaaa-aaaa-aaaa-aaaa',
				userName: 'testUser',
			}
		});

		// Act
		const userData = {
			userId: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'testUser',
		};
		await hlsEncoderChecker.updateEncodeStatus(userData);

		// Assert
		expect(mockSongRepository.save).toHaveBeenCalledWith([
			{ 
				encodeJobId: 'xxxx-xxxx-xxxx-xxxx',
				processingStatus: 5,
				durationInMs: 10000,
			}
		]);

		expect(mockNotificationQueue.produceNotificationMessage).toHaveBeenCalledWith(
			[
				{ 
					value: JSON.stringify({
						publishUserId: 'aaaa-aaaa-aaaa-aaaa',
						message: 'testUser has just publish a new song: test song',
					})
				}
			]
		);

		expect(mockUserCache.deleteUserSongs).toHaveBeenCalledWith(userData.userId);
	});

	test('happy case : should execute songRepository.save with processingStatu and delete userSongs cache ENCODE_FAILED if hlsEncodeJob ERROR', async () => {
		// Arrange
		mockSongRepository.find.mockResolvedValue([
			{
				encodeJobId: 'xxxx-xxxx-xxxx-xxxx',
			},
		]);

		mockHlsEncoderService.getJob.mockResolvedValue({
			$metadata: { httpStatusCode: 200 },
			Job: {
				Status: 'ERROR',
			}
		});

		// Act
		const userData = {
			userId: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'testUser',
		};
		await hlsEncoderChecker.updateEncodeStatus(userData);

		// Assert
		expect(mockSongRepository.save).toHaveBeenCalledWith([
			{ 
				encodeJobId: 'xxxx-xxxx-xxxx-xxxx',
				processingStatus: 4,
			}
		]);

		expect(mockUserCache.deleteUserSongs).toHaveBeenCalledWith(userData.userId);
	});

	test('bad caes : should return if hlsEncodeJob httpStatusCode is not 200', async () => {
		// Arrange
		mockSongRepository.find.mockResolvedValue([
			{
				encodeJobId: 'xxxx-xxxx-xxxx-xxxx',
			},
		]);

		mockHlsEncoderService.getJob.mockResolvedValue({
			$metadata: { httpStatusCode: 500 },
		});

		// Act
		const userData = {
			userId: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'testUser',
		};
		const result = await hlsEncoderChecker.updateEncodeStatus(userData);

		// Assert
		expect(mockHlsEncoderService.getJob).toHaveBeenCalledTimes(1);
		expect(result).toBeUndefined();
	});

	test('bad case : should execute songRepository.save with [] if getHlsEncodeResult occur error', async () => {
		// Arrange
		mockSongRepository.find.mockResolvedValue([
			{
				encodeJobId: 'xxxx-xxxx-xxxx-xxxx',
			},
		]);

		mockHlsEncoderService.getJob.mockRejectedValue(new Error('test error'));

		// Act
		const userData = {
			userId: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'testUser',
		};
		const result = await hlsEncoderChecker.updateEncodeStatus(userData);

		// Assert
		expect(mockHlsEncoderService.getJob).toHaveBeenCalledTimes(1);
		expect(result).toBeUndefined();
	});
});
