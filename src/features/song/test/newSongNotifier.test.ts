import { notificationSocket } from '../../../global/sockets/notification.socket';
import { NewSongNotifier } from '../services/newSongNotifier';

jest.mock('../../../global/sockets/notification.socket', () => ({
	notificationSocket: {
		emit: jest.fn(),
	},
}));

let newSongNotifier: NewSongNotifier;

describe('NewSongNotifier', () => {
	beforeEach(() => {
		jest.resetAllMocks();
		newSongNotifier = new NewSongNotifier();
	});

	test('happy case : sendNewSongNotification should emit newSongNotification event', async () => {
		const message = {
			value: JSON.stringify({
				publishUserId: 'aaaa-aaaa-aaaa-aaaa',
				message: 'testUser has just publish a new song: test song',
			}),
		};

		await newSongNotifier.sendNewSongNotification({ message });

		expect(notificationSocket.emit).toHaveBeenCalledWith(
			'newSongNotification',
			{
				publishUserId: 'aaaa-aaaa-aaaa-aaaa',
				message: 'testUser has just publish a new song: test song',
			}
		);
	});
});

// expect(notificationSocket.emit).toHaveBeenCalledWith(
// 	'newSongNotifications',
// 	[
// 		{
// 			publishUserId: 'aaaa-aaaa-aaaa-aaaa',
// 			message: 'testUser has just publish a new song: test song',
// 		}
// 	]
// );