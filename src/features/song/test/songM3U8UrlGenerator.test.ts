import { NotFoundError } from '../../../global/utils/customError';
import { SongM3U8UrlGenerator } from '../services/songM3U8UrlGenerator';
import { StorageService } from '../../../global/service/storage/storage';
import { SongRepository } from '../../../global/service/db/song.db';

const mockStorageService = {
	getObjectSignedUrl: jest.fn()
};

const mockSongRepository = {
	findOne: jest.fn()
};

let songM3U8UrlGenerator: SongM3U8UrlGenerator;

describe('SongM3U8UrlGenerator', () => {
	beforeEach(() => {
		jest.resetAllMocks();
		songM3U8UrlGenerator = new SongM3U8UrlGenerator(
      mockStorageService as unknown as StorageService,
      mockSongRepository as unknown as SongRepository
		);
	});

	test('happy case : should return encodedSongFileObjectUrl', async () => {
		// Arrange
		mockSongRepository.findOne.mockResolvedValue({
			id: 'xxxx-xxxx-xxxx-xxxx',
			songStorageDetail: { awsS3BucketName: 'example-bucket', encodedSongFileKey: 'example/file/key' },
		});

		mockStorageService.getObjectSignedUrl.mockResolvedValue('https://example.com/example/file/key');

		// Act
		const result = await songM3U8UrlGenerator.generate('xxxx-xxxx-xxxx-xxxx');

		// Assert
		expect(result).toBe('https://example.com/example/file/key');
	});

	test('bad case : should throw NotFoundError if song is null', async () => {
		// Arrange
		mockSongRepository.findOne.mockResolvedValue(null);

		// Act and Assert
		expect(songM3U8UrlGenerator.generate('xxxx-xxxx-xxxx-xxxx')).rejects.toThrow(NotFoundError);
	});
});
