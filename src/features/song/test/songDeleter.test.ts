import { ForbiddenError, InternalServerError, NotFoundError } from '../../../global/utils/customError';
import { SongDeleter } from '../services/songDeleter';
import { SongRepository } from '../../../global/service/db/song.db';
import { StorageService } from '../../../global/service/storage/storage';
import { UserCache } from '../../../global/cache/user.cache';

const mockSongRepository = {
	findOne: jest.fn(),
	delete: jest.fn(),
};

const mockStorageService = {
	listObjects: jest.fn(),
	deleteObjects: jest.fn(),
};

const mockUserCache = {
	deleteUserSongs: jest.fn(),
};

let songDeleter: SongDeleter;

describe('SongDeleter', () => {
	beforeEach(() => {
		jest.resetAllMocks();
		songDeleter = new SongDeleter(
      mockSongRepository as unknown as SongRepository,
      mockStorageService as unknown as StorageService,
			mockUserCache as unknown as UserCache
		);
	});

	test('happy case : songRepository.delete() should have been called once', async () => {
		// Arrange
		mockSongRepository.findOne.mockResolvedValue({
			id: 'xxxx-xxxx-xxxx-xxxx',
			user: { id: 'yyyy-yyyy-yyyy-yyyy' },
			songStorageDetail: { awsS3BucketName: 'example-bucket', fileDestination: 'example/file/destination' },
		});

		mockStorageService.listObjects.mockResolvedValue({
			$metadata: { httpStatusCode: 200 },
			Contents: [{ Key: 'example/file/destination/test.m3u8' }],
		});

		mockStorageService.deleteObjects.mockResolvedValue({
			$metadata: { httpStatusCode: 200 },
		});

		// Act
		const mockUserData = {
			userId: 'yyyy-yyyy-yyyy-yyyy',
			userName: 'test'
		};
		const mockSongId = 'xxxx-xxxx-xxxx-xxxx';
		await songDeleter.delete(mockUserData, mockSongId);

		// Assert
		expect(mockSongRepository.delete).toHaveBeenCalledWith('xxxx-xxxx-xxxx-xxxx');

		expect(mockUserCache.deleteUserSongs).toHaveBeenCalledWith(mockUserData.userId);
	});

	test('bad case : should throw NotFoundError if songToDelete is null', async () => {
		// Arrange
		mockSongRepository.findOne.mockResolvedValue(null);

		// Act and Assert
		const mockUserData = {
			userId: 'yyyy-yyyy-yyyy-yyyy',
			userName: 'test'
		};
		const mockSongId = 'xxxx-xxxx-xxxx-xxxx';
		expect(songDeleter.delete(mockUserData, mockSongId)).rejects.toThrow(NotFoundError);
	});

	test('bad case : should throw ForbiddenError if user is not authorized to delete the song', async () => {
		// Arrange
		mockSongRepository.findOne.mockResolvedValue({
			id: 'xxxx-xxxx-xxxx-xxxx',
			user: { id: 'zzzz-zzzz-zzzz-zzzz' },
			songStorageDetail: { awsS3BucketName: 'example-bucket', fileDestination: 'example/file/destination' },
		});

		// Act and Assert
		const mockUserData = {
			userId: 'yyyy-yyyy-yyyy-yyyy',
			userName: 'test'
		};
		const mockSongId = 'xxxx-xxxx-xxxx-xxxx';
		expect(songDeleter.delete(mockUserData, mockSongId)).rejects.toThrow(ForbiddenError);
	});

	test('bad case : should throw InternalServerError if failed to get song files', async () => {
		// Arrange
		mockSongRepository.findOne.mockResolvedValue({
			id: 'xxxx-xxxx-xxxx-xxxx',
			user: { id: 'yyyy-yyyy-yyyy-yyyy' },
			songStorageDetail: { awsS3BucketName: 'example-bucket', fileDestination: 'example/file/destination' },
		});

		mockStorageService.listObjects.mockResolvedValue({
			$metadata: { httpStatusCode: 500 },
		});

		// Act and Assert
		const mockUserData = {
			userId: 'yyyy-yyyy-yyyy-yyyy',
			userName: 'test'
		};
		const mockSongId = 'xxxx-xxxx-xxxx-xxxx';
		expect(songDeleter.delete(mockUserData, mockSongId)).rejects.toThrow(InternalServerError);
	});

	test('bad case : should throw NotFoundError if song files does not exist', async () => {
		// Arrange
		mockSongRepository.findOne.mockResolvedValue({
			id: 'xxxx-xxxx-xxxx-xxxx',
			user: { id: 'yyyy-yyyy-yyyy-yyyy' },
			songStorageDetail: { awsS3BucketName: 'example-bucket', fileDestination: 'example/file/destination' },
		});

		mockStorageService.listObjects.mockResolvedValue({
			$metadata: { httpStatusCode: 200 },
		});

		// Act and Assert
		const mockUserData = {
			userId: 'yyyy-yyyy-yyyy-yyyy',
			userName: 'test'
		};
		const mockSongId = 'xxxx-xxxx-xxxx-xxxx';
		expect(songDeleter.delete(mockUserData, mockSongId)).rejects.toThrow(NotFoundError);
	});

	test('bad case : should throw InternalServerError if failed to delete song files', async () => {
		// Arrange
		mockSongRepository.findOne.mockResolvedValue({
			id: 'xxxx-xxxx-xxxx-xxxx',
			user: { id: 'yyyy-yyyy-yyyy-yyyy' },
			songStorageDetail: { awsS3BucketName: 'example-bucket', fileDestination: 'example/file/destination' },
		});

		mockStorageService.listObjects.mockResolvedValue({
			$metadata: { httpStatusCode: 200 },
			Contents: [{ Key: 'example/file/destination/test.m3u8' }],
		});

		mockStorageService.deleteObjects.mockResolvedValue({
			$metadata: { httpStatusCode: 500 },
		});

		// Act and Assert
		const mockUserData = {
			userId: 'yyyy-yyyy-yyyy-yyyy',
			userName: 'test'
		};
		const mockSongId = 'xxxx-xxxx-xxxx-xxxx';
		expect(songDeleter.delete(mockUserData, mockSongId)).rejects.toThrow(InternalServerError);
	});
});
