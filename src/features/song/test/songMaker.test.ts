import { UploadedFile } from 'express-fileupload';
import { config } from '../../../config';
import { SongMaker } from '../services/songMaker';
import { NotFoundError, InternalServerError } from '../../../global/utils/customError';
import { SONG_PROCESSING_STATUS } from '../enum/song.enum';
import { SongFacroty } from '../services/songFactory';
import { SongStorageDetailFactory } from '../services/songStorageDetailFactory';
import { StorageService } from '../../../global/service/storage/storage';
import { HlsEncoderService } from '../../../global/service/hlsEncoder/hlsEncoder';
import { UserRepository } from '../../../global/service/db/user.db';
import { SongRepository } from '../../../global/service/db/song.db';
import { SongStorageDetailRepository } from '../../../global/service/db/songStorageDetail.db';
import { UserCache } from '../../../global/cache/user.cache';

const mockStorageService = {
	uploadFile: jest.fn(),
};

const mockHlsEncoderService = {
	encode: jest.fn(),
};

const mockUserRepository = {
	findOneBy: jest.fn(),
};

const mockSongRepository = {
	create: jest.fn(),
	update: jest.fn(),
};

const mockSongStorageDetailRepository = {
	create: jest.fn(),
};

const mockSongFacroty = {
	createSong: jest.fn(),
};

const mockSongStorageDetailFactory = {
	createSongStorageDetail: jest.fn(),
};

const mockUserCache = {
	deleteUserSongs: jest.fn(),
};

jest.spyOn(Date, 'now').mockReturnValue(1689975660000);

describe('songMaker', () => {
	let songMaker: SongMaker;

	beforeEach(() => {
		jest.resetAllMocks();
		songMaker = new SongMaker(
      mockStorageService as unknown as StorageService,
      mockHlsEncoderService as unknown as HlsEncoderService,
      mockUserRepository as unknown as UserRepository,
      mockSongRepository as unknown as SongRepository,
			mockSongStorageDetailRepository as unknown as SongStorageDetailRepository,
      mockSongFacroty as unknown as SongFacroty,
			mockSongStorageDetailFactory as unknown as SongStorageDetailFactory,
			mockUserCache as unknown as UserCache,
		);
	});

	test('happy case : should create song and call encode function and delete userSongs cache', async () => {
		// Arrange
		mockUserRepository.findOneBy.mockResolvedValue({
			id: 'xxxx-xxxx-xxxx-xxxx'
		});

		mockSongFacroty.createSong.mockReturnValue({
			songName: 'testSong',
			processingStatus: SONG_PROCESSING_STATUS.UPLOADING,
			user: { id: 'xxxx-xxxx-xxxx-xxxx' },
		});

		mockSongRepository.create.mockResolvedValue({ 
			id: 'xxxx-xxxx-xxxx-xxxx',
			songName: 'testSong',
			processingStatus: SONG_PROCESSING_STATUS.UPLOADING,
			user: { id: 'xxxx-xxxx-xxxx-xxxx' },
		});

		mockStorageService.uploadFile.mockResolvedValue({
			$metadata: {
				httpStatusCode: 200,
			}
		});

		mockSongStorageDetailFactory.createSongStorageDetail.mockReturnValue({
			song: { id: 'xxxx-xxxx-xxxx-xxxx' },
			storageType: 's3',
			awsRegion: 'ap-northeast-1',
			awsS3BucketName: 'music-share',
			fileDestination: `${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/`,
			originalSongFileKey: `${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/original.mp4`,
			encodedSongFileKey: `${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/hls/originalhls.m3u8`,
		});

		mockSongStorageDetailRepository.create.mockResolvedValue({
			id: 'xxxx-xxxx-xxxx-xxxx',
			song: { id: 'xxxx-xxxx-xxxx-xxxx' },
			storageType: 's3',
			awsRegion: 'ap-northeast-1',
			awsS3BucketName: 'music-share',
			fileDestination: `${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/`,
			originalSongFileKey: `${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/original.mp4`,
			encodedSongFileKey: `${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/hls/originalhls.m3u8`,
		});

		mockHlsEncoderService.encode.mockResolvedValue({
			$metadata: {
				httpStatusCode: 201,
			},
			Job: {
				id: 'xxxx-xxxx-xxxx-xxxx',
			}
		});

		(Date.now as jest.Mock).mockReturnValue(1689975660000);

		// Act
		const mockUserData = {
			userId: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'testUser',
		};

		const mockSongName = 'testSong';

		const mockSongFile = {
			name: 'testSong.mp4',
			data: Buffer.from('testSong'),
		};

		await songMaker.create(
			mockUserData,
			mockSongName,
      mockSongFile as unknown as UploadedFile,
		);

		// Assert
		expect(mockUserRepository.findOneBy).toHaveBeenCalledWith({
			id: mockUserData.userId
		});

		expect(mockSongFacroty.createSong).toHaveBeenCalledWith(
			mockSongName,
			SONG_PROCESSING_STATUS.UPLOADING,
			{ id: mockUserData.userId }
		);

		expect(mockSongRepository.create).toHaveBeenCalledWith({
			songName: 'testSong',
			processingStatus: SONG_PROCESSING_STATUS.UPLOADING,
			user: { id: 'xxxx-xxxx-xxxx-xxxx' },
		});

		expect(mockStorageService.uploadFile).toHaveBeenCalledWith({
			name: `${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/original.mp4`,
			data: Buffer.from('testSong'),
		});

		expect(mockSongStorageDetailFactory.createSongStorageDetail).toHaveBeenCalledWith(
			{ 
				id: 'xxxx-xxxx-xxxx-xxxx',
				songName: 'testSong',
				processingStatus: SONG_PROCESSING_STATUS.UPLOADING,
				user: { id: 'xxxx-xxxx-xxxx-xxxx' },
			},
			's3',
			'ap-northeast-1',
			'music-share',
			`${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/`,
			`${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/original.mp4`,
			`${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/hls/originalhls.m3u8`,
		);

		expect(mockSongStorageDetailRepository.create).toHaveBeenCalledWith({
			song: { id: 'xxxx-xxxx-xxxx-xxxx' },
			storageType: 's3',
			awsRegion: 'ap-northeast-1',
			awsS3BucketName: 'music-share',
			fileDestination: `${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/`,
			originalSongFileKey: `${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/original.mp4`,
			encodedSongFileKey: `${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/hls/originalhls.m3u8`,
		});

		expect(mockHlsEncoderService.encode).toHaveBeenCalledWith(
			`s3://music-share/${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/original.mp4`,
			`s3://music-share/${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/hls/`,
		);

		expect(mockSongRepository.update).toHaveBeenCalledTimes(2);

		expect(mockUserCache.deleteUserSongs).toHaveBeenCalledWith(mockUserData.userId);
	});

	test('bad case : should throw NotFoundError if user does not exist', async () => {
		// Arrange
		mockUserRepository.findOneBy.mockResolvedValue(null);

		// Act & Assert
		const mockUserData = {
			userId: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'testUser',
		};

		const mockSongName = 'testSong';

		const mockSongFile = {
			name: 'testSong.mp4',
			data: Buffer.from('testSong'),
		};

		expect(
			songMaker.create(
				mockUserData,
				mockSongName,
				mockSongFile as unknown as UploadedFile
			)
		).rejects.toThrow(NotFoundError);
	});

	test('bad case : should throw InternalServerError if upload failed', async () => {
		// Arrange
		mockUserRepository.findOneBy.mockResolvedValue({
			id: 'xxxx-xxxx-xxxx-xxxx'
		});

		mockSongFacroty.createSong.mockReturnValue({
			songName: 'testSong',
			processingStatus: SONG_PROCESSING_STATUS.UPLOADING,
			user: { id: 'xxxx-xxxx-xxxx-xxxx' },
		});

		mockSongRepository.create.mockResolvedValue({ 
			id: 'xxxx-xxxx-xxxx-xxxx',
			songName: 'testSong',
			processingStatus: SONG_PROCESSING_STATUS.UPLOADING,
			user: { id: 'xxxx-xxxx-xxxx-xxxx' },
		});

		mockStorageService.uploadFile.mockResolvedValue({
			$metadata: {
				httpStatusCode: 500,
			}
		});

		// Act & Assert
		const mockUserData = {
			userId: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'testUser',
		};

		const mockSongName = 'testSong';

		const mockSongFile = {
			name: 'testSong.mp4',
			data: Buffer.from('testSong'),
		};

		expect(
			songMaker.create(
				mockUserData,
				mockSongName,
				mockSongFile as unknown as UploadedFile
			)
		).rejects.toThrow(InternalServerError);
	});

	test('bad case : should throw InternalServerError if encode failed', async () => {
		// Arrange
		mockUserRepository.findOneBy.mockResolvedValue({
			id: 'xxxx-xxxx-xxxx-xxxx'
		});

		mockSongFacroty.createSong.mockReturnValue({
			songName: 'testSong',
			processingStatus: SONG_PROCESSING_STATUS.UPLOADING,
			user: { id: 'xxxx-xxxx-xxxx-xxxx' },
		});

		mockSongRepository.create.mockResolvedValue({ 
			id: 'xxxx-xxxx-xxxx-xxxx',
			songName: 'testSong',
			processingStatus: SONG_PROCESSING_STATUS.UPLOADING,
			user: { id: 'xxxx-xxxx-xxxx-xxxx' },
		});

		mockStorageService.uploadFile.mockResolvedValue({
			$metadata: {
				httpStatusCode: 200,
			}
		});

		mockSongStorageDetailFactory.createSongStorageDetail.mockReturnValue({
			song: { id: 'xxxx-xxxx-xxxx-xxxx' },
			storageType: 's3',
			awsRegion: 'ap-northeast-1',
			awsS3BucketName: 'music-share',
			fileDestination: `${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/`,
			originalSongFileKey: `${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/original.mp4`,
			encodedSongFileKey: `${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/hls/originalhls.m3u8`,
		});

		mockSongStorageDetailRepository.create.mockResolvedValue({
			id: 'xxxx-xxxx-xxxx-xxxx',
			song: { id: 'xxxx-xxxx-xxxx-xxxx' },
			storageType: 's3',
			awsRegion: 'ap-northeast-1',
			awsS3BucketName: 'music-share',
			fileDestination: `${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/`,
			originalSongFileKey: `${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/original.mp4`,
			encodedSongFileKey: `${config.ENVIRONMENT}/xxxx-xxxx-xxxx-xxxx/testSong-1689975660000/hls/originalhls.m3u8`,
		});

		mockHlsEncoderService.encode.mockResolvedValue({
			$metadata: {
				httpStatusCode: 500,
			}
		});

		(Date.now as jest.Mock).mockReturnValue(1689975660000);

		// Act & Assert
		const mockUserData = {
			userId: 'xxxx-xxxx-xxxx-xxxx',
			userName: 'testUser',
		};

		const mockSongName = 'testSong';

		const mockSongFile = {
			name: 'testSong.mp4',
			data: Buffer.from('testSong'),
		};

		expect(
			songMaker.create(
				mockUserData,
				mockSongName,
				mockSongFile as unknown as UploadedFile
			)
		).rejects.toThrow(InternalServerError);
	});
});
