import HTTP_STATUS from 'http-status-codes';
import { ForbiddenError, InternalServerError, NotFoundError } from '../../../global/utils/customError';
import { IUser } from '../../auth/interfaces/auth.interfaces';
import { ISongToDelete } from '../interfaces/songDeleter.interface';
import { StorageService, storageService } from '../../../global/service/storage/storage';
import { User } from '../../../entities/user.entity';
import { SongRepository, songRepository } from '../../../global/service/db/song.db';
import { UserCache, userCache } from '../../../global/cache/user.cache';
 
export class SongDeleter {
	private songRepository: SongRepository;
	private storageService: StorageService;
	private userCache: UserCache;

	constructor(songRepository: SongRepository, storageService: StorageService, userCache: UserCache) {
		this.songRepository = songRepository;
		this.storageService = storageService;
		this.userCache = userCache;
	}

	public async delete(userData: IUser, songId: string): Promise<void> {
		const songToDelete = await this.getSongToDelete(songId);
		if (!songToDelete) { throw new NotFoundError('Song does not exist'); }
		const { user, songStorageDetail } = songToDelete;
		const { awsS3BucketName, fileDestination } = songStorageDetail;

		const isAuthorizedToDelete = this.getIsAuthorizedToDelete(userData, user);
		if (!isAuthorizedToDelete) { throw new ForbiddenError('Have no authorized to delete this resource.'); }
		
		const songfileKeys = await this.getSongFileKeys(awsS3BucketName, fileDestination);
		await this.deleteSongFiles(awsS3BucketName, songfileKeys);
		await this.deleteSong(songId);
		await this.deleteUserSongsCache(userData.userId);
	}

	private async getSongToDelete(songId: string): Promise<ISongToDelete | null> {
		const songToDelete = this.songRepository.findOne({
			select: {
				id: true,
				user: {
					id: true,
				},
				songStorageDetail: {
					awsS3BucketName: true,
					fileDestination: true,
				},
			},
			where: { id: songId },
			relations: ['user', 'songStorageDetail'],
		});

		return songToDelete;
	}

	private getIsAuthorizedToDelete(userData: IUser, user: User): boolean {
		const { userId: requestUserId } = userData;
		const { id: songUserId } = user;

		return requestUserId === songUserId;
	}

	private async getSongFileKeys(awsS3BucketName: string, fileDestiantion: string): Promise<{ Key: string }[]> {
		const listObjectsResult = await this.storageService.listObjects(awsS3BucketName, fileDestiantion);
		if (listObjectsResult.$metadata.httpStatusCode !== HTTP_STATUS.OK) { throw new InternalServerError('Failed to get song files'); }
		const { Contents } = listObjectsResult;

		if (Contents === undefined) { throw new NotFoundError('Song files does not exist'); }
	  const songFileKeys = Contents.map(item => ({ Key: item.Key || '' }));

		return songFileKeys;
	}

	private async deleteSongFiles(awsS3BucketName: string, songfileKeys: { Key: string }[]): Promise<void> {
		const deleteSongFilesResult = await this.storageService.deleteObjects(awsS3BucketName, songfileKeys);
		if (deleteSongFilesResult.$metadata.httpStatusCode !== HTTP_STATUS.OK) { throw new InternalServerError('Failed to delete song files'); }
	}

	private async deleteSong(songId: string): Promise<void> {
		await this.songRepository.delete(songId);
	}

	private async deleteUserSongsCache(userId: string): Promise<void> {
		await this.userCache.deleteUserSongs(userId);
	}
}

export const songDeleter = new SongDeleter(songRepository, storageService, userCache);
