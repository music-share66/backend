import { NotFoundError } from '../../../global/utils/customError';
import { ISong } from '../interfaces/songM3U8UrlGenerator.interface';
import { StorageService, storageService } from '../../../global/service/storage/storage';
import { SongRepository, songRepository } from '../../../global/service/db/song.db';

export class SongM3U8UrlGenerator {
	private storageService: StorageService;
	private songRepository: SongRepository;

	constructor(storageService: StorageService, songRepository: SongRepository) {
		this.storageService = storageService;
		this.songRepository = songRepository;
	}
  
	public async generate(songId: string): Promise<string> {
		const song = await this.getSongBySongId(songId);
		if (!song) { throw new NotFoundError('Song not found'); }

		const { songStorageDetail } = song;
		const { awsS3BucketName, encodedSongFileKey } = songStorageDetail;
		const encodedSongFileObjectUrl = await this.getEncodedSongFileObjectUrl(awsS3BucketName, encodedSongFileKey);

		return encodedSongFileObjectUrl;
	}

	private async getSongBySongId(songId: string): Promise<ISong | null>{
		const song = await this.songRepository.findOne({
			select: {
				id: true,
				songStorageDetail: {
					awsRegion: true,
					awsS3BucketName: true,
					encodedSongFileKey: true
				}
			},
			where: { id: songId },
			relations: ['songStorageDetail']
		});

		return song;
	}

	private async getEncodedSongFileObjectUrl(awsS3BucketName: string, encodedSongFileKey: string) {
		return await this.storageService.getObjectSignedUrl(awsS3BucketName, encodedSongFileKey);
	}
}

export const songM3U8UrlGenerator = new SongM3U8UrlGenerator(storageService, songRepository);
