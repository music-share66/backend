import { SongStorageDetail } from '../../../entities/songStorageDetail.entity';
import { Song } from '../../../entities/song.entity';

export class SongStorageDetailFactory {
	createSongStorageDetail(
		song: Song,
		storageType: string,
		awsRegion: string,
		awsS3BucketName: string,
		fileDestination: string,
		originalSongFileKey: string,
		encodedSongFileKey: string
	) {
		const songStorageDetail = new SongStorageDetail();

		songStorageDetail.song = song;
		songStorageDetail.storageType = storageType;
		songStorageDetail.awsRegion = awsRegion;
		songStorageDetail.awsS3BucketName = awsS3BucketName;
		songStorageDetail.fileDestination = fileDestination;
		songStorageDetail.originalSongFileKey = originalSongFileKey;
		songStorageDetail.encodedSongFileKey = encodedSongFileKey;

		return songStorageDetail;
	}
}

export const songStorageDetailFactory = new SongStorageDetailFactory();
