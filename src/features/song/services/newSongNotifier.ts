import { Message } from 'kafkajs';
import { notificationSocket } from '../../../global/sockets/notification.socket';

export class NewSongNotifier {
	public async sendNewSongNotification({ message }: { message: Message }): Promise<void> {
		const newSongNotification = message.value ? JSON.parse(message.value.toString()) : null;
		notificationSocket.emit('newSongNotification', newSongNotification);
	}
}

export const newSongNotifier = new NewSongNotifier();
