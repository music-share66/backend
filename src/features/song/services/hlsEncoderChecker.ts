import HTTP_STATUS from 'http-status-codes';
import { IUpdateProperties } from '../../../global/interfaces/repository.interface';
import { hlsEncoderService, HlsEncoderService } from '../../../global/service/hlsEncoder/hlsEncoder';
import { NotificationQueue, notificationQueue } from '../../../global/queue/notification.queue';
import { SONG_PROCESSING_STATUS } from '../enum/song.enum';
import { Song } from '../../../entities/song.entity';
import { SongRepository, songRepository } from '../../../global/service/db/song.db';
import { UserCache, userCache } from '../../../global/cache/user.cache';
import { INewSongNotification, ISongOwner } from '../interfaces/hlsEncoderChecker.interface';
import { IUser } from '../../auth/interfaces/auth.interfaces';

export class HlsEncoderChecker {
	private hlsEncoderService: HlsEncoderService;
	private songRepository: SongRepository;
	private notificationQueue: NotificationQueue;
	private userCache: UserCache;

	constructor(hlsEncoderService: HlsEncoderService, songRepository: SongRepository, notificationQueue: NotificationQueue, userCache: UserCache) {
		this.hlsEncoderService = hlsEncoderService;
		this.songRepository = songRepository;
		this.notificationQueue = notificationQueue;
		this.userCache = userCache;
	}

	public async updateEncodeStatus(userData: IUser): Promise<void> {
		const { userId } = userData;
		const songs = await this.getSongsByUserIdProcessingStatus(userId, SONG_PROCESSING_STATUS.ENCODING);
		if (songs.length === 0) { return; }
		const hlsEncodeResults = await this.getHlsEncodeResults(songs);
		if (hlsEncodeResults.length === 0) { return; }

		await this.updateSong(hlsEncodeResults);
		await this.sendNewSongNotification(hlsEncodeResults);
		await this.deleteUserSongsCache(userId);
	}

	private async getSongsByUserIdProcessingStatus(userId: string, processingStatus: number): Promise<Song[]> {
		return await this.songRepository.find({
			where: {
				user: { id: userId },
				processingStatus
			}
		});
	}

	private async getHlsEncodeResults(songs: Song[]) {
		const hlsEncodeResults = [];
		for (const song of songs) {
			const hlsEncodeResult = await this.getHlsEncodeResult(song);
			if (!hlsEncodeResult) { continue; }

			hlsEncodeResults.push(hlsEncodeResult);
		}

		return hlsEncodeResults;      
	}

	private async getHlsEncodeResult(song: Song): Promise<Song | null> {
		try {
			const { encodeJobId } = song;
			const hlsEncodeJob = await this.hlsEncoderService.getJob(encodeJobId as string);
			if (hlsEncodeJob.$metadata.httpStatusCode !== HTTP_STATUS.OK) { return null; }

			const jobStatus = hlsEncodeJob.Job?.Status;
			if (jobStatus === 'COMPLETE') {
				const durationInMs = hlsEncodeJob.Job?.OutputGroupDetails?.[0].OutputDetails?.[0].DurationInMs;

				this.setUpdateSong(song, {
					processingStatus: SONG_PROCESSING_STATUS.COMPLETE,
					durationInMs: durationInMs as number,
				});
			} else if (jobStatus === 'ERROR') {
				this.setUpdateSong(song, {
					processingStatus: SONG_PROCESSING_STATUS.ENCODE_FAILED
				});
			}
			
			return song;
		} catch (error) {
			return null;
		}
	}

	private setUpdateSong(song: Song, updateProperties: IUpdateProperties): void {
		Object.assign(song, updateProperties);
	}

	private async updateSong(hlsEncodeResults: Song[]): Promise<void> {
		await this.songRepository.save(hlsEncodeResults);
	}

	private async sendNewSongNotification(hlsEncodeResultsFiltered: Song[]): Promise<void> {
		const newSongNotifications = await this.getNewSongNotifications(hlsEncodeResultsFiltered);
		await this.notificationQueue.produceNotificationMessage(newSongNotifications);
	}

	private async getNewSongNotifications(hlsEncodeResultsFiltered: Song[]): Promise<INewSongNotification[]> {
		const newSongNotifications = await Promise.all(hlsEncodeResultsFiltered.map(async (song): Promise<INewSongNotification | null> => {
			const { id: songId } = song;
			const songOwner = await this.getSongOwnerById(songId as string);
			if (!songOwner) { return null; }

			const { songName, user } = songOwner;
			const { id: userId, userName } = user;

			const newSongNotification = {
				value: JSON.stringify({
					publishUserId: userId,
					message: `${userName} has just publish a new song: ${songName}`,
				})
			};

			return newSongNotification;			
		}));

		return newSongNotifications as INewSongNotification[];
	}

	private async getSongOwnerById(songId: string): Promise<ISongOwner | null> {
		const songOwner =  await this.songRepository.findOne({
			select: {
				id: true,
				songName: true,
				user: {
					id: true,
					userName: true,
				}
			},
			where: { id: songId },
			relations: ['user']
		});

		return songOwner;
	}

	private async deleteUserSongsCache(userId: string): Promise<void> {
		await this.userCache.deleteUserSongs(userId);
	}
}

export const hlsEncoderChecker = new HlsEncoderChecker(hlsEncoderService, songRepository, notificationQueue, userCache);
