import { Song } from '../../../entities/song.entity';
import { User } from '../../../entities/user.entity';

export class SongFacroty {
	public createSong(songName: string, processingStatus: number, user: User): Song {
		const song = new Song();
		song.songName = songName;
		song.processingStatus = processingStatus;
		song.user = user;

		return song;
	}
}

export const songFacroty = new SongFacroty();
