import { UploadedFile } from 'express-fileupload';
import HTTP_STATUS from 'http-status-codes';
import { config } from '../../../config';
import { NotFoundError, InternalServerError } from '../../../global/utils/customError';
import { IUser } from '../../auth/interfaces/auth.interfaces';
import { IUploadSongResult, IEncodeResult } from '../interfaces/songMaker.interfaces';
import { SONG_PROCESSING_STATUS } from '../enum/song.enum';
import { SongFacroty, songFacroty } from './songFactory';
import { SongStorageDetailFactory, songStorageDetailFactory } from './songStorageDetailFactory';
import { IUpdateProperties } from '../../../global/interfaces/repository.interface';
import { StorageService, storageService, IFile } from '../../../global/service/storage/storage';
import { hlsEncoderService, HlsEncoderService } from '../../../global/service/hlsEncoder/hlsEncoder';
import { User } from '../../../entities/user.entity';
import { Song } from '../../../entities/song.entity';
import { SongStorageDetail } from '../../../entities/songStorageDetail.entity';
import { UserRepository, userRepository } from '../../../global/service/db/user.db';
import { SongRepository, songRepository } from '../../../global/service/db/song.db';
import { SongStorageDetailRepository, songStorageDetailRepository } from '../../../global/service/db/songStorageDetail.db';
import { UserCache, userCache } from '../../../global/cache/user.cache';

export class SongMaker {
	private storageService: StorageService;
	private hlsEncoderService: HlsEncoderService;
	private userRepository: UserRepository;
	private songRepository: SongRepository;
	private songStorageDetailRepository: SongStorageDetailRepository;
	private songFacroty: SongFacroty;
	private songStorageDetailFactory: SongStorageDetailFactory;
	private userCache: UserCache;

	constructor(
		storageService: StorageService,
		hlsEncoderService: HlsEncoderService,
		userRepository: UserRepository,
		songRepository: SongRepository,
		songStorageDetailRepository: SongStorageDetailRepository,
		songFacroty: SongFacroty,
		songStorageDetailFactory: SongStorageDetailFactory,
		userCache: UserCache,
	) {
		this.storageService = storageService;
		this.hlsEncoderService = hlsEncoderService;
		this.userRepository = userRepository;
		this.songRepository = songRepository;
		this.songStorageDetailRepository = songStorageDetailRepository;
		this.songFacroty = songFacroty;
		this.songStorageDetailFactory = songStorageDetailFactory;
		this.userCache = userCache;
	}

	public async create(userData: IUser, songName: string, songFile: UploadedFile ): Promise<void> {
		const { userId } = userData;
		const { name, data } = songFile;
		const user = await this.getUserById(userId);
		if (!user) { throw new NotFoundError('User does not exist'); }

		// create song data in database
		const newSong = await this.createSong(songName, SONG_PROCESSING_STATUS.UPLOADING, user);
		
		// upload song to storage then update uploadSongResult to song in database
		const { processingStatus, songStorageDetail } = await this.uploadSong(newSong, userId, songName, name, data);
		await this.updateSong(newSong.id, { processingStatus });
		if (processingStatus === SONG_PROCESSING_STATUS.UPLOAD_FAILED || !songStorageDetail) { throw new InternalServerError('Failed to upload song'); }
		const newSongStorageDetail = await this.createSongStorageDetail(songStorageDetail);
		const { storageType, awsS3BucketName, originalSongFileKey } = newSongStorageDetail;

		// trigger aws media convert then update encodeResult to song data in database
		const hlsEncodeResult = await this.hlsEncode(storageType, awsS3BucketName, originalSongFileKey);
		await this.updateSong(newSong.id, { ...hlsEncodeResult });
		if (hlsEncodeResult.processingStatus === SONG_PROCESSING_STATUS.ENCODE_FAILED) { throw new InternalServerError('Failed to encode song'); }

		await this.deleteUserSongsCache(userId);
	}

	private async getUserById(userId: string): Promise<User |  null> {
		return await this.userRepository.findOneBy({ id: userId });
	}

	private async createSong(songName: string, processingStatus: number, user: User): Promise<Song> {
		const song = this.songFacroty.createSong(songName, processingStatus, user);
		const newSong = await this.songRepository.create(song);
	
		return newSong;
	}

	private async uploadSong(newSong: Song, userId: string, songName: string, name: string, data: Buffer): Promise<IUploadSongResult> {
		const uploadSongResult = {
			processingStatus: SONG_PROCESSING_STATUS.UPLOAD_FAILED,
			songStorageDetail: null,
		};

		const uriEncodedSongName = this.getUriEncodedSongName(songName);
		const fileExtension = this.getFileExtension(name);
		const { fileDestination, file } = this.getFile(userId, uriEncodedSongName, fileExtension as string, data);
		const encodedSongFileKey = this.getEncodedSongFileKey(file.name);
		const uploadFileResult = await this.storageService.uploadFile(file);
		this.setUploadSongResult(
			newSong,
			uploadFileResult.$metadata.httpStatusCode as number,
			uploadSongResult,
			's3',
			config.AWS_REGION as string,
			config.AWS_BUCKET_NAME as string,
			fileDestination,
			file.name,
			encodedSongFileKey
		);

		return uploadSongResult;
	}

	private getUriEncodedSongName(songName: string): string {
		return encodeURIComponent(songName);
	}

	private getFileExtension(fileName: string): string | undefined {
		return fileName.split('.').pop();
	}

	private getFile(userId: string, uriEncodedSongName: string, fileExtension: string, data: Buffer): { fileDestination: string, file: IFile } {
		const fileDestination = `${config.ENVIRONMENT}/${userId}/${uriEncodedSongName}-${Date.now()}/`;
		const file = {
			name: `${fileDestination}original.${fileExtension}`,
			data
		};

		return { 
			fileDestination,
			file
		};
	}

	private getEncodedSongFileKey(originalSongFileUrl: string): string{
		const splitOriginalSongFileUrl = originalSongFileUrl.split('/');
		const insertIndex = splitOriginalSongFileUrl.length - 1;
		splitOriginalSongFileUrl.splice(insertIndex, 0, 'hls');
		const finalUrl = splitOriginalSongFileUrl.join('/');
		const encodedSongFileUrl = finalUrl.replace(/\.mp4$/, 'hls.m3u8');

		return encodedSongFileUrl;
	}

	private setUploadSongResult(
		newSong: Song,
		httpStatusCode: number,
		uploadSongResult: IUploadSongResult,
		storageType: string,
		awsRegion: string,
		awsS3BucketName: string,
		fileDestination: string,
		fileName: string,
		encodedSongFileKey: string
	): void {
		if (httpStatusCode === HTTP_STATUS.OK) {
			const songStorageDetail = this.songStorageDetailFactory.createSongStorageDetail(
				newSong,
				storageType,
				awsRegion,
				awsS3BucketName,
				fileDestination,
				fileName,
				encodedSongFileKey
			);

			uploadSongResult.processingStatus = SONG_PROCESSING_STATUS.ENCODING;
			uploadSongResult.songStorageDetail = songStorageDetail;
		}
	}

	private async createSongStorageDetail(songStorageDetail: SongStorageDetail): Promise<SongStorageDetail> {
		return await this.songStorageDetailRepository.create(songStorageDetail);
	}

	private async hlsEncode(storageType: string, awsS3BucketName: string, originalSongFileKey: string): Promise<IEncodeResult> {
		const hlsEncodeResult: IEncodeResult = {
			processingStatus: SONG_PROCESSING_STATUS.ENCODE_FAILED,
			encodeJobId: null,
		};

		const originalSongFileUrl = this.getOriginalSongFileUrl(storageType, awsS3BucketName, originalSongFileKey);
		const encodedFileDestination = this.getEncodedFileDestination(storageType, awsS3BucketName, originalSongFileKey);
		const encodeResult = await this.hlsEncoderService.encode(originalSongFileUrl, encodedFileDestination);
		this.setHlsEncodeResult(encodeResult.$metadata.httpStatusCode as number, hlsEncodeResult, encodeResult.Job?.Id as string);

		return hlsEncodeResult;
	}

	private getOriginalSongFileUrl(storageType: string, awsS3BucketName: string, originalSongFileKey: string): string {
		return `${storageType}://${awsS3BucketName}/${originalSongFileKey}`;
	}

	private getEncodedFileDestination(storageType: string, awsS3BucketName: string, originalSongFileKey: string): string {
		const originalSongFileDestination = originalSongFileKey.split('/').slice(0, -1).join('/');
		const hlsFileFolder = 'hls/';
		const destination = `${storageType}://${awsS3BucketName}/${originalSongFileDestination}/${hlsFileFolder}`;

		return destination;
	}

	private setHlsEncodeResult(httpStatusCode: number, hlsEncodeResult: IEncodeResult, encodeJobId: string): void {
		if (httpStatusCode === HTTP_STATUS.CREATED) {
			hlsEncodeResult.processingStatus = SONG_PROCESSING_STATUS.ENCODING;
			hlsEncodeResult.encodeJobId = encodeJobId;
		}
	}

	private async updateSong(songId: string, updateProprties: IUpdateProperties): Promise<void> {
		await this.songRepository.update(songId, updateProprties);
	}

	private async deleteUserSongsCache(userId: string): Promise<void> {
		await this.userCache.deleteUserSongs(userId);
	}
}

export const songMaker = new SongMaker(
	storageService,
	hlsEncoderService,
	userRepository,
	songRepository,
	songStorageDetailRepository,
	songFacroty,
	songStorageDetailFactory,
	userCache,
);
