import dotenv from 'dotenv';
dotenv.config();

class Config {
	public PORT: string | undefined;
	public API_DOC_ENVIRONMENT: string | undefined;
	public ENVIRONMENT: string | undefined;
	public CLIENT_HOST: string | undefined;
	public DB_TYPE: string | undefined;
	public DB_HOST: string | undefined;
	public DB_PORT: string | undefined;
	public DB_USERNAME: string | undefined;
	public DB_PASSWORD: string | undefined;
	public DB_DATABASE: string | undefined;
	public JWT_SECRET_KEY: string | undefined;
	public GOOGLE_CLIENT_ID: string | undefined;
	public GOOGLE_CLIENT_SECRET: string | undefined;
	public GOOGLE_OAUTH_URL: string | undefined;
	public GOOGLE_CALL_BACK_URL: string | undefined;
	public GOOGLE_ACCESS_TOKEN_URL: string | undefined;
	public GOOGLE_TOKEN_INFO_URL: string | undefined;
	public AWS_ACCESS_KEY: string | undefined;
	public AWS_ACCESS_SECRET: string | undefined;
	public AWS_REGION: string | undefined;
	public AWS_BUCKET_NAME: string | undefined;
	public KAFKA_BROKER: string | undefined;
	public KAFKA_USERNAME: string | undefined;
	public KAFKA_PASSWORD: string | undefined;
	public REDIS_HOST: string | undefined;

	constructor() {
		this.PORT = process.env.PORT;
		this.API_DOC_ENVIRONMENT = process.env.API_DOC_ENVIRONMENT;
		this.ENVIRONMENT = process.env.ENVIRONMENT;
		this.CLIENT_HOST = process.env.CLIENT_HOST;
		this.DB_TYPE = process.env.DB_TYPE;
		this.DB_HOST = process.env.DB_HOST;
		this.DB_PORT = process.env.DB_PORT;
		this.DB_USERNAME = process.env.DB_USERNAME;
		this.DB_PASSWORD = process.env.DB_PASSWORD;
		this.DB_DATABASE = process.env.DB_DATABASE;
		this.JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;
		this.GOOGLE_CLIENT_ID = process.env.GOOGLE_CLIENT_ID;
		this.GOOGLE_CLIENT_SECRET = process.env.GOOGLE_CLIENT_SECRET;
		this.GOOGLE_OAUTH_URL = process.env.GOOGLE_OAUTH_URL;
		this.GOOGLE_CALL_BACK_URL = process.env.GOOGLE_CALL_BACK_URL;
		this.GOOGLE_ACCESS_TOKEN_URL = process.env.GOOGLE_ACCESS_TOKEN_URL;
		this.GOOGLE_TOKEN_INFO_URL = process.env.GOOGLE_TOKEN_INFO_URL;
		this.AWS_ACCESS_KEY = process.env.AWS_ACCESS_KEY;
		this.AWS_ACCESS_SECRET = process.env.AWS_ACCESS_SECRET;
		this.AWS_REGION = process.env.AWS_REGION;
		this.AWS_BUCKET_NAME = process.env.AWS_BUCKET_NAME;
		this.KAFKA_BROKER = process.env.KAFKA_BROKER;
		this.KAFKA_USERNAME = process.env.KAFKA_USERNAME;
		this.KAFKA_PASSWORD = process.env.KAFKA_PASSWORD;
		this.REDIS_HOST = process.env.REDIS_HOST;
	}

	public validateConfig() {
		for (const [key, value] of Object.entries(this)) {
			if (value === undefined) {
				throw new Error(`Configuration ${key} is undefined.`);
			}
		}
	}
}

export const config = new Config();