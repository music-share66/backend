import http from 'http';
import fs from 'fs';
import path from 'path';
import YAML from 'yaml';
import { Server } from 'socket.io';
import fileUpload from 'express-fileupload';
import swaggerUi from 'swagger-ui-express';
import cors from 'cors';
import { Application, json, urlencoded, Request, Response, NextFunction } from 'express';
import { IErrorResponse, CustomError, NotFoundError, InternalServerError } from './global/utils/customError';
import logger from './global/utils/winston';
import { config } from './config';
import applicationRoutes from './routes';
import { NotificationSocketHandler } from './global/sockets/notification.socket';
import { notificationQueue } from './global/queue/notification.queue';

export class MusicshareServer {
	private app: Application;

	constructor(app: Application) {
		this.app = app;
	}

	public start(): void {
		this.securityMiddleware(this.app);
		this.standardMiddleware(this.app);
		this.routeMiddleware(this.app);
		this.apiDocument(this.app);
		this.errorHandler(this.app);
		this.startServer(this.app);
	}

	private securityMiddleware(app: Application): void {
		app.use(
			cors({
				origin: config.CLIENT_HOST,
				credentials: true,
				methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS']
			})
		);
	}

	private standardMiddleware(app: Application): void {
		app.use(json());
		app.use(urlencoded({ extended: true }));
		app.use(fileUpload());
	}

	private routeMiddleware(app: Application): void {
		applicationRoutes(app);
	}

	private apiDocument(app: Application): void {
		if (config.API_DOC_ENVIRONMENT === 'localhost') {
			const swaggerDocuments: string[] = [];

			// Read api.yaml file in the apidoc folder
			const apiYamlFile = fs.readFileSync(path.join('apidoc', 'api.yaml'), 'utf8');
			const componentsYamlFile = fs.readFileSync(path.join('apidoc', 'components.yaml'), 'utf8');
			swaggerDocuments.push(YAML.parse(apiYamlFile));
			swaggerDocuments.push(YAML.parse(componentsYamlFile));

			// Read all yaml files in the apidoc/api folder
			const filePath = 'apidoc/api';
			const files = fs.readdirSync(filePath);
			const api = {
				paths: {}
			};
			files.forEach(file => {
				if (file.endsWith('.yaml')) {
					const yamlFile = fs.readFileSync(path.join(filePath, file), 'utf8');
					Object.assign(api.paths, YAML.parse(yamlFile).paths);
				}
			});

			const mergedSwaggerDocument = {};
			Object.assign(mergedSwaggerDocument, ...swaggerDocuments);
			Object.assign(mergedSwaggerDocument, api);

			app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(mergedSwaggerDocument));
		}
	}

	private errorHandler(app: Application): void {
		app.all('*', (req: Request, res: Response) => {
			const notFoundError = new NotFoundError(`${req.originalUrl} not found`);
			res.status(notFoundError.statusCode).json(notFoundError.serializeErrors());
		});

		app.use((err: IErrorResponse, _req: Request, res: Response, next: NextFunction) => {
			logger.error([err.statusCode, err.message, err.stack.split('\n')[1].trim()]);
			if (err instanceof CustomError) {
				res.status(err.statusCode).json(err.serializeErrors());
			} else {
				const internalServerError = new InternalServerError(err.message);
				res.status(internalServerError.statusCode).json(internalServerError.serializeErrors());
			}

			next();
		});
	}

	private startServer(app: Application): void {
		const httpServer = new http.Server(app);
		const socketIo = this.createSocketIo(httpServer);
		this.startHttpServer(httpServer);
		this.socketIoConnections(socketIo);
		this.queueConnections();
	}

	private createSocketIo(httpServer: http.Server): Server {
		const io = new Server(httpServer, {
			cors: {
				origin: config.CLIENT_HOST,
				credentials: true,
				methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS']
			}
		});

		return io;
	}

	private startHttpServer(httpServer: http.Server): void {
		httpServer.listen(config.PORT, () => {
			logger.info(`Server is running on port ${config.PORT}`);
		});
	}

	private socketIoConnections(io: Server): void {
		const notificationSocketHandler = new NotificationSocketHandler(io);
		notificationSocketHandler.listen();
	}

	private queueConnections(): void {
		notificationQueue.consumeNotificationMessage();
	}
}