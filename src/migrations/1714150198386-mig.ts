import { MigrationInterface, QueryRunner } from "typeorm";

export class Mig1714150198386 implements MigrationInterface {
    name = 'Mig1714150198386'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "song" DROP COLUMN "duration"`);
        await queryRunner.query(`ALTER TABLE "song" ADD "duration_in_ms" integer`);
        await queryRunner.query(`ALTER TABLE "song" ADD "file_destination" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "song" DROP COLUMN "file_destination"`);
        await queryRunner.query(`ALTER TABLE "song" DROP COLUMN "duration_in_ms"`);
        await queryRunner.query(`ALTER TABLE "song" ADD "duration" character varying`);
    }

}
