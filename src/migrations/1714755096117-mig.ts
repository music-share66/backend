import { MigrationInterface, QueryRunner } from "typeorm";

export class Mig1714755096117 implements MigrationInterface {
    name = 'Mig1714755096117'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "song_storage_detail" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "storage_type" character varying NOT NULL, "aws_region" character varying NOT NULL, "aws_s3_bucket_name" character varying NOT NULL, "file_destination" character varying NOT NULL, "original_song_file_key" character varying NOT NULL, "encoded_song_file_key" character varying, "create_date" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "update_date" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "song_id" uuid, CONSTRAINT "REL_9c3cf8e7d98d70c1a7febdf50b" UNIQUE ("song_id"), CONSTRAINT "PK_9c9331f00cacedfd94425880915" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "song_storage_detail" ADD CONSTRAINT "FK_9c3cf8e7d98d70c1a7febdf50b2" FOREIGN KEY ("song_id") REFERENCES "song"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "song_storage_detail" DROP CONSTRAINT "FK_9c3cf8e7d98d70c1a7febdf50b2"`);
        await queryRunner.query(`DROP TABLE "song_storage_detail"`);
    }

}
