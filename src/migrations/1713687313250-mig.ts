import { MigrationInterface, QueryRunner } from "typeorm";

export class Mig1713687313250 implements MigrationInterface {
    name = 'Mig1713687313250'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "song" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "song_name" character varying NOT NULL, "duration" character varying, "original_song_file_url" character varying, "encoded_song_file_url" character varying, "processing_status" integer NOT NULL, "create_date" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "update_date" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "user_id" uuid, CONSTRAINT "PK_baaa977f861cce6ff954ccee285" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "song" ADD CONSTRAINT "FK_d3d3c3bb3980d5575dd5bfef302" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "song" DROP CONSTRAINT "FK_d3d3c3bb3980d5575dd5bfef302"`);
        await queryRunner.query(`DROP TABLE "song"`);
    }

}
