import { MigrationInterface, QueryRunner } from "typeorm";

export class Mig1713886758927 implements MigrationInterface {
    name = 'Mig1713886758927'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "song" ADD "encode_job_id" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "song" DROP COLUMN "encode_job_id"`);
    }

}
