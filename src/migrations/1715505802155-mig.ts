import { MigrationInterface, QueryRunner } from "typeorm";

export class Mig1715505802155 implements MigrationInterface {
    name = 'Mig1715505802155'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "follow_user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "create_date" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "follower_id" uuid, "followee_id" uuid, CONSTRAINT "PK_d3b514cd26ff6190a8f836f9b28" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "follow_user" ADD CONSTRAINT "FK_054fb00b2541321f9daa8750f1b" FOREIGN KEY ("follower_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "follow_user" ADD CONSTRAINT "FK_36026d709540d031bb20ab0324a" FOREIGN KEY ("followee_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "follow_user" DROP CONSTRAINT "FK_36026d709540d031bb20ab0324a"`);
        await queryRunner.query(`ALTER TABLE "follow_user" DROP CONSTRAINT "FK_054fb00b2541321f9daa8750f1b"`);
        await queryRunner.query(`DROP TABLE "follow_user"`);
    }

}
