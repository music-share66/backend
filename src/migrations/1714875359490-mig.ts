import { MigrationInterface, QueryRunner } from "typeorm";

export class Mig1714875359490 implements MigrationInterface {
    name = 'Mig1714875359490'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "song" DROP COLUMN "original_song_file_url"`);
        await queryRunner.query(`ALTER TABLE "song" DROP COLUMN "encoded_song_file_url"`);
        await queryRunner.query(`ALTER TABLE "song" DROP COLUMN "file_destination"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "song" ADD "file_destination" character varying`);
        await queryRunner.query(`ALTER TABLE "song" ADD "encoded_song_file_url" character varying`);
        await queryRunner.query(`ALTER TABLE "song" ADD "original_song_file_url" character varying`);
    }

}
