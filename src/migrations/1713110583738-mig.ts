import { MigrationInterface, QueryRunner } from "typeorm";

export class Mig1713110583738 implements MigrationInterface {
    name = 'Mig1713110583738'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "password_hash"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "password_hash" text`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "id_token"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "id_token" text`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "refresh_token"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "refresh_token" text`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "refresh_token"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "refresh_token" character varying`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "id_token"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "id_token" character varying`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "password_hash"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "password_hash" character varying`);
    }

}
