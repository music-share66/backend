import { DataSource } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import logger from './global/utils/winston';
import { config } from './config';

export const dataSource = new DataSource({
	type: config.DB_TYPE as 'postgres',
	host: config.DB_HOST,
	port: config.DB_PORT as unknown as number,
	username: config.DB_USERNAME,
	password: config.DB_PASSWORD,
	database: config.DB_DATABASE,
	entities: ['src/entities/*{.js,.ts}'],
	migrations: ['src/migrations/*{.js,.ts}'],
	namingStrategy: new SnakeNamingStrategy(),
	logging: false,
	synchronize: false,
	migrationsRun: false,
	ssl: config.ENVIRONMENT === 'localhost' ? false : true,
	extra: config.ENVIRONMENT === 'localhost' ? {} : {ssl: { rejectUnauthorized: false }},
});

export const connectDatabase = () => {
	dataSource.initialize()
		.then(() => {
			logger.info('Data Source has been initialized!');
		})
		.catch((err) => {
			logger.error(`Error during Data Source initialization: ${err}`);
		});
};