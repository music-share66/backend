import { Server, Socket } from 'socket.io';
import logger from '../utils/winston';

export let notificationSocket: Server;

export class NotificationSocketHandler {
	private io: Server;

	constructor(io: Server) {
		this.io = io;
	}

	public listen(): void {
		notificationSocket = this.io;

		this.io.on('connection', (socket: Socket) => {
			socket.emit('notificationSocketConnect', 'Notification socket connected');

			socket.on('notificationSocketConnect', (data) => {
				logger.info(data);
			});
		});
	}
}
