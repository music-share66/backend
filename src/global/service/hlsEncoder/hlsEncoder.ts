import {
	MediaConvertClient,
	CreateJobCommand,
	GetJobCommand,
	AccelerationMode,
	TimecodeSource,
	AudioDefaultSelection,
	OutputGroupType
} from '@aws-sdk/client-mediaconvert';
import { config } from '../../../config';

export class HlsEncoderService {
	private hlsEncoderClient: MediaConvertClient;
  
	constructor(mediaConvertClient: MediaConvertClient) {
		this.hlsEncoderClient = mediaConvertClient;
	}

	public async encode(fileInput: string, destination: string) {
		const params = {
			Role: 'arn:aws:iam::232330796615:role/service-role/MediaConvert_Default_Role',
			AccelerationSettings: {
				Mode: AccelerationMode.DISABLED,
			},		
			Settings : {
				TimecodeConfig: {
					Source: TimecodeSource.ZEROBASED,
				},
				FollowSource: 1,
				Inputs: [
					{
						AudioSelectors: {
							'Audio Selector 1': {
								DefaultSelection: AudioDefaultSelection.DEFAULT
							}
						},
						VideoSelector: {},
						TimecodeSource:  TimecodeSource.ZEROBASED,
						FileInput: fileInput,
					},
				],
				OutputGroups: [
					{
						Name: 'Apple HLS',
						OutputGroupSettings: {
							Type: OutputGroupType.HLS_GROUP_SETTINGS,
							HlsGroupSettings: {
								Destination: destination,
								MinSegmentLength: 0,
								SegmentLength: 10,
							},
						},
						Outputs: [
							{
								NameModifier: 'hls',
								Preset: 'System-Avc_16x9_270p_14_99fps_400kbps',
							},
						],
					},
				],
			}
		};

		const createJobCommand = new CreateJobCommand(params);
		const data = await this.hlsEncoderClient.send(createJobCommand);
		return data;
	}

	public async getJob(jobId: string) {
		const getJobCommand = new GetJobCommand({ Id: jobId });
		const job = await this.hlsEncoderClient.send(getJobCommand);

		return job;
	}
}

const mediaConvertConfig = { region: config.AWS_REGION };
const mediaConvertClient = new MediaConvertClient(mediaConvertConfig);

export const hlsEncoderService = new HlsEncoderService(mediaConvertClient);
