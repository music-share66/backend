import { EntityTarget, ObjectLiteral } from 'typeorm';
import { DataSource } from 'typeorm';

export class Repository<E extends ObjectLiteral> {
	protected repository;

	constructor(dataSource: DataSource, entity: EntityTarget<E>) {
		this.repository = dataSource.getRepository(entity);
	}
}
