import { DataSource, EntityTarget, FindOneOptions, FindManyOptions } from 'typeorm';
import { dataSource } from '../../../setupDatabase';
import { Repository } from './repository';
import { ICondition, IUpdateProperties } from '../../interfaces/repository.interface';
import { Song } from '../../../entities/song.entity';

export class SongRepository extends Repository<Song> {
	constructor(dataSource: DataSource, song: EntityTarget<Song>) {
		super(dataSource, song);
	}
 
	public async create(song: Song): Promise<Song> {
		return this.repository.save(song);
	}

	public async save(songs: Song[]): Promise<Song[]> {
		return this.repository.save(songs);
	}
 
	public async findOneBy(condition: ICondition): Promise<Song | null> {
		return this.repository.findOneBy(condition);
	}

	public async findOne(condition: FindOneOptions): Promise<Song | null> {
		return this.repository.findOne(condition);
	}

	public async find(condition: FindManyOptions): Promise<Song[]> {
		return this.repository.find(condition);
	}

	public async update(id: string, updateProprties: IUpdateProperties): Promise<void> {
		this.repository.update(id, updateProprties);
	}

	public async delete(id: string): Promise<void> {
		this.repository.delete(id);
	}
}

export const songRepository = new SongRepository(dataSource, Song);
