import { DataSource, EntityTarget, FindOneOptions, FindManyOptions } from 'typeorm';
import { dataSource } from '../../../setupDatabase';
import { Repository } from './repository'; 
import { User } from '../../../entities/user.entity';

interface ICondition {
  [key: string]: string | number | boolean;
}

export class UserRepository extends Repository<User> {
	constructor(dataSource: DataSource, user: EntityTarget<User>) {
		super(dataSource, user);
	}
 
	public async create(user: User): Promise<User> {
		return this.repository.save(user);
	}

	public async findOneBy(condition: ICondition): Promise<User | null> {
		return this.repository.findOneBy(condition);
	}

	public async findOne(condition: FindOneOptions): Promise<User | null> {
		return this.repository.findOne(condition);
	}

	public async find(condition: FindManyOptions): Promise<User[]> {
		return this.repository.find(condition);
	}
}

export const userRepository = new UserRepository(dataSource, User);
