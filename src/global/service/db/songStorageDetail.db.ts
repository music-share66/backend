import { DataSource, EntityTarget } from 'typeorm';
import { dataSource } from '../../../setupDatabase';
import { Repository } from './repository';
import { IUpdateProperties } from '../../interfaces/repository.interface'; 
import { SongStorageDetail } from '../../../entities/songStorageDetail.entity';

export class SongStorageDetailRepository extends Repository<SongStorageDetail> {
	constructor(dataSource: DataSource, songStorageDetail: EntityTarget<SongStorageDetail>) {
		super(dataSource, songStorageDetail);
	}
 
	public async create(songStorageDetail: SongStorageDetail): Promise<SongStorageDetail> {
		return this.repository.save(songStorageDetail);
	}

	public async save(songStorageDetails: SongStorageDetail[]): Promise<SongStorageDetail[]> {
		return this.repository.save(songStorageDetails);
	}

	public async update(id: string, updateProprties: IUpdateProperties): Promise<void> {
		this.repository.update(id, updateProprties);
	}
}

export const songStorageDetailRepository = new SongStorageDetailRepository(dataSource, SongStorageDetail);
