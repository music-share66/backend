import { DataSource, EntityTarget, FindOneOptions } from 'typeorm';
import { dataSource } from '../../../setupDatabase';
import { Repository } from './repository'; 
import { FollowUser } from '../../../entities/followUser.entity';

export class FollowUserRepository extends Repository<FollowUser> {
	constructor(dataSource: DataSource, followUser: EntityTarget<FollowUser>) {
		super(dataSource, followUser);
	}
 
	public async create(followUser: FollowUser): Promise<FollowUser> {
		return this.repository.save(followUser);
	}

	public async findOne(condition: FindOneOptions): Promise<FollowUser | null> {
		return this.repository.findOne(condition);
	}

	public async delete(followUser: FollowUser): Promise<void> {
		this.repository.delete(followUser);
	}
}

export const followUserRepository = new FollowUserRepository(dataSource, FollowUser);
