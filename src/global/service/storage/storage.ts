import { S3Client, PutObjectCommand, GetObjectCommand, ListObjectsV2Command, DeleteObjectsCommand, ObjectIdentifier } from '@aws-sdk/client-s3';
import { config } from '../../../config';
import { getSignedUrl } from '@aws-sdk/s3-request-presigner';

export interface IFile {
  name: string;
  data: Buffer;
}

export class StorageService {
	private storageClient: S3Client;

	constructor(storageClient: S3Client) {
		this.storageClient = storageClient;
	}

	public async uploadFile(file: IFile) {
		const putObjectCommand = new PutObjectCommand({
			Bucket: config.AWS_BUCKET_NAME,
			Key: file.name,
			Body: file.data,
		});
		const result = await this.storageClient.send(putObjectCommand);

		return result;
	}

	public async getObjectSignedUrl(awsS3BucketName: string, objectKey: string) {
		const getObjectCommand = new GetObjectCommand({
			Bucket: awsS3BucketName,
			Key: objectKey,
		});
		const signedUrl = getSignedUrl(s3Client, getObjectCommand);

		return signedUrl;
	}

	public async listObjects(awsS3BucketName: string, fileDestiantion: string) {
		const listObjectCommand = new ListObjectsV2Command({
			Bucket: awsS3BucketName,
			Prefix: fileDestiantion,
		});
		const listObjectsResult = await this.storageClient.send(listObjectCommand);

		return listObjectsResult;
	}

	public async deleteObjects(awsS3BucketName: string, objectKeys: ObjectIdentifier[]) {
		const deleteObjectsCommand = new DeleteObjectsCommand({
			Bucket: awsS3BucketName,
			Delete: {
				Objects: objectKeys
			}
		});
		const deleteObjectsResult = await s3Client.send(deleteObjectsCommand);

		return deleteObjectsResult;
	}
}

const s3Config = {
	accessKeyId: config.AWS_ACCESS_KEY,
	secretAccessKey: config.AWS_ACCESS_SECRET,
	region: config.AWS_REGION,
};

const s3Client = new S3Client(s3Config);

export const storageService = new StorageService(s3Client);
