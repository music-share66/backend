export interface ICondition {
  [key: string]: null | string | number | boolean;
}

export interface IUpdateProperties {
	[key: string]: null | string | number | boolean;
}
