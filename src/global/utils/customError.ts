import HTTP_STATUS from 'http-status-codes';

export interface IError {
	message: string;
	statusCode: number;
	status: string;
}

export interface IErrorResponse {
	message: string;
	statusCode: number;
	status: string;
	stack: string;
	serializeErrors(): IError;
}

export abstract class CustomError extends Error {
  abstract statusCode: number;
	abstract status: string;

	constructor(message: string) {
		super(message);
	}

	serializeErrors(): IError {
		return {
			message: this.message,
			statusCode: this.statusCode,
			status: this.status,
		};
	}
}

// 400
export class BadRequestError extends CustomError {
	statusCode = HTTP_STATUS.BAD_REQUEST;
	status = 'error';

	constructor(message: string) {
		super(message);
	}
}

// 401
export class UnAuthorizedError extends CustomError {
	statusCode = HTTP_STATUS.UNAUTHORIZED;
	status = 'error';

	constructor(message: string) {
		super(message);
	}
}

// 403
export class ForbiddenError extends CustomError {
	statusCode = HTTP_STATUS.FORBIDDEN;
	status = 'error';

	constructor(message: string) {
		super(message);
	}
}

// 404
export class NotFoundError extends CustomError {
	statusCode = HTTP_STATUS.NOT_FOUND;
	status = 'error';

	constructor(message: string) {
		super(message);
	}
}

// 409
export class ConflictError extends CustomError {
	statusCode = HTTP_STATUS.CONFLICT;
	status = 'error';

	constructor(message: string) {
		super(message);
	}
}

// 500
export class InternalServerError extends CustomError {
	statusCode = HTTP_STATUS.INTERNAL_SERVER_ERROR;
	status = 'error';

	constructor(message: string) {
		super(message);
	}
}

// 503
export class ServiceUnavailableError extends CustomError {
	statusCode = HTTP_STATUS.SERVICE_UNAVAILABLE;
	status = 'error';

	constructor(message: string) {
		super(message);
	}
}

export class FileTooLargeError extends CustomError {
	statusCode = HTTP_STATUS.REQUEST_TOO_LONG;
	status = 'error';

	constructor(message: string) {
		super(message);
	}
}
