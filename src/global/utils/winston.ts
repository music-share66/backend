import winston from 'winston';
// import { config } from '../config';

const logger = winston.createLogger({
	level: 'info',
	format: winston.format.combine(
		winston.format.errors({ stack: true }),
		winston.format.json()
	),
	transports: [
		new winston.transports.File({ filename: 'error.log', level: 'error' }),
	],
});


logger.add(new winston.transports.Console({
	format: winston.format.simple(),
}));

export default logger;