import { Request, Response, NextFunction } from 'express';
import { Schema } from 'joi';
import { BadRequestError } from '../utils/customError';

export class SchemaValidateMiddleware {
	static validateBody(schema: Schema) {
		return (req: Request, _res: Response, next: NextFunction): void => {
			const { error } = schema.validate(req.body);
			if (error) {
				throw new BadRequestError(error.message);
			}

			next();
		};
	}

	static validateFile(schema: Schema) {
		return (req: Request, _res: Response, next: NextFunction): void => {
			const { error } = schema.validate(req.files);
			if (error) {
				throw new BadRequestError(error.message);
			}

			next();
		};
	}
}
