import axios from 'axios';
import { Request, Response, NextFunction } from 'express';
import cookie from 'cookie';
import jwt, { JwtPayload } from 'jsonwebtoken';
import { OAuth2Client } from 'google-auth-library';
import { config } from '../../config';
import { UnAuthorizedError } from '../utils/customError';
import { User } from '../../entities/user.entity';
import { userRepository } from '../service/db/user.db';

interface IVerifiedUser {
	userId: string;
	userName: string;
}

export class AuthMiddleware {
	static verifyUser() {
		return async (req: Request, _res: Response, next: NextFunction): Promise<void> => {
			try {
				const cookies = cookie.parse(req.headers.cookie || '');
				const { Authorization } = cookies;
				if (!Authorization) { throw new UnAuthorizedError('Authorization token is required'); }

				const jwtToken = Authorization.split(' ')[1];
				const verifiedJwtPayload = jwt.verify(jwtToken, config.JWT_SECRET_KEY as string);
				if (!verifiedJwtPayload) { throw new UnAuthorizedError('Invalid token'); }

				// use different auth strategy depents on the authType
				const authMiddlewareInstance = new AuthMiddleware();
				const authType = (verifiedJwtPayload as JwtPayload).authType;
				const verifiedUser = await authMiddlewareInstance.getVerifiedUser(authType, verifiedJwtPayload as JwtPayload);
				req.user = verifiedUser;

				next();
			} catch (error) {
				next(error);
			}
		};
	}

	private async getVerifiedUser(authType: string, verifiedJwt: JwtPayload): Promise<IVerifiedUser> {
		let verifiedUser: IVerifiedUser;
		switch (authType) {
			case 'local':
				verifiedUser = this.loacalAuth(verifiedJwt);
				break;

			case 'google':
				verifiedUser = await this.googleAuth(verifiedJwt);
				break;

			default:
				throw new UnAuthorizedError('Invalid authType');
		}

		return verifiedUser;
	}

	private loacalAuth(verifiedJwt: JwtPayload): IVerifiedUser {
		const verifiedUser = {
			userId: (verifiedJwt as JwtPayload).userId,
			userName: (verifiedJwt as JwtPayload).userName,
		};

		return verifiedUser;
	}

	private async googleAuth(verifiedJwt: JwtPayload): Promise<IVerifiedUser> {
		const verifiedUser = {
			userId: '',
			userName: '',
		};

		// get idToken from db
		const userId = (verifiedJwt as JwtPayload).userId;
		const user = await userRepository.findOneBy({ id: userId });
		if (!user) { throw new UnAuthorizedError('Invalid user'); }

		// check if idToken is valid
		let idToken = user.idToken;
		const refreshToken = user.refreshToken;
		const isGoogleIdTokenValid = await this.getIsGoogleIdTokenValid(idToken as string);
		if (!isGoogleIdTokenValid) {
			const newGoogleIdToken = await this.getNewGoogleIdToken(refreshToken as string);
			await this.updateGoogleIdToken(user, newGoogleIdToken);
			idToken = newGoogleIdToken;
		}

		const googleUserName = await this.getGoogleUserName(idToken as string);
		verifiedUser.userId = userId;
		verifiedUser.userName = googleUserName;

		return verifiedUser;
	}

	private async getIsGoogleIdTokenValid(idToken: string): Promise<boolean> {
		const oAuth2Client = new OAuth2Client();
		const ticket = await oAuth2Client.verifyIdToken({
			idToken: idToken as string,
			audience: config.GOOGLE_CLIENT_ID,
		})
			.then((ticket) => ticket)
			.catch(() => {
				return false;
			});

		return ticket ? true : false;
	}

	private async getNewGoogleIdToken(refreshToken: string): Promise<string> {
		const googleRefreshTokenUrl = config.GOOGLE_ACCESS_TOKEN_URL;
		const data = {
			access_type: 'offline',
			refresh_token: refreshToken,
			client_id: config.GOOGLE_CLIENT_ID,
			client_secret: config.GOOGLE_CLIENT_SECRET,
			grant_type: 'refresh_token',
		};
		const accessTokenData = await axios.post(`${googleRefreshTokenUrl}`, data)
			.then((res) => res.data)
			.catch((err) => {
				throw new UnAuthorizedError(err.message);
			});

		const { id_token: newGoogleIdToken } = accessTokenData;

		return newGoogleIdToken;
	}

	private async updateGoogleIdToken(user: User, newGoogleIdToken: string): Promise<void> {
		user.idToken = newGoogleIdToken;
		await userRepository.create(user);
	}

	private async getGoogleUserName(idToken: string): Promise<string> {
		const googleUserDataUrl = `${config.GOOGLE_TOKEN_INFO_URL}?id_token=${idToken}`;
		const googleUserData = await axios.get(`${googleUserDataUrl}`)
			.then((res) => res.data)
			.catch((err) => {
				throw new UnAuthorizedError(err.message);
			});

		const { name: userName } = googleUserData;
		return userName;
	}
}
