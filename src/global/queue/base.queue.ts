import { Kafka, Admin, logLevel, ProducerRecord, EachMessageHandler } from 'kafkajs';
import { config } from '../../config';

export class BaseQueue {
	private kafka: Kafka;
	private admin: Admin;

	constructor() {
		this.kafka = new Kafka({
			brokers: [`${config.KAFKA_BROKER}`],
			ssl: true,
			sasl: {
				mechanism: 'plain',
				username: `${config.KAFKA_USERNAME}`,
				password: `${config.KAFKA_PASSWORD}`,
			},
			logLevel: logLevel.ERROR,
		});
		this.admin = this.kafka.admin();
	}

	protected async produceMessage(topic: string, messages: ProducerRecord['messages']): Promise<void> {
		const producer = this.kafka.producer();
		await producer.connect();
		await producer.send({ topic, messages });
		await producer.disconnect();
	}

	protected async consumeMessage(groupId: string, topic: string, callback: EachMessageHandler): Promise<void> {
		const consumer = this.kafka.consumer({ groupId });
		await consumer.connect();
		await consumer.subscribe({ topic, fromBeginning: true });
		await consumer.run({
			eachMessage: callback,
		});
	}

	protected async createTopic(topic: string): Promise<void> {
		await this.admin.connect();
		const topics = await this.admin.listTopics();
		if (!topics.includes(topic)) {
			await this.admin.createTopics({
				topics: [
					{
						topic,
						numPartitions: 1,
						replicationFactor: 1,
					},
				],
			});
		}
		await this.admin.disconnect();
	}
}
