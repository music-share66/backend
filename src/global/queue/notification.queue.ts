import { ProducerRecord, EachMessageHandler } from 'kafkajs';
import { config } from '../../config';
import { BaseQueue } from './base.queue';
import { newSongNotifier } from '../../features/song/services/newSongNotifier';

export class NotificationQueue extends BaseQueue {
	private groupId: string;
	private topic: string;

	constructor() {
		super();
		this.topic = `${config.ENVIRONMENT}_notification`;
		this.groupId = `${config.ENVIRONMENT}_notification_group_${Date.now()}`;
	}

	public async produceNotificationMessage(messages: ProducerRecord['messages']): Promise<void>{
		await this.produceMessage(this.topic, messages);
	}

	public async consumeNotificationMessage(): Promise<void>{
		await this.createTopic(this.topic);
		await this.consumeMessage(
			this.groupId,
			this.topic,
      newSongNotifier.sendNewSongNotification as unknown as EachMessageHandler,
		);
	}
}

export const notificationQueue = new NotificationQueue();
