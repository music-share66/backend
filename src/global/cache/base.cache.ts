import { createClient } from 'redis';
import logger from '../utils/winston';
import { config } from '../../config';

export type RedisClient = ReturnType<typeof createClient>;

export class BaseCache {
	client: RedisClient;

	constructor(cacheName: string) {
		this.client = createClient({ url: config.REDIS_HOST });
		this.cacheError(cacheName);
	}

	private cacheError(cacheName: string): void {
		this.client.on('error', (error: unknown) => {
			logger.error(`Error in ${cacheName} cache: ${error}`);
		});
	}
}
