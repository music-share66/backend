import { BaseCache } from './base.cache';
import { IUserSongs } from '../../features/user/interfaces/userSongs.interfaces';

export class UserCache extends BaseCache {
	constructor() {
		super('User');
	}

	public async setUserSongs(userId: string, userSongs: IUserSongs) {
		if (!this.client.isOpen) {
			await this.client.connect();
		}

		await this.client.HSET(`user:${userId}`, 'userSongs', JSON.stringify(userSongs));
	}

	public async getUserSongs(userId: string): Promise<IUserSongs> {
		if (!this.client.isOpen) {
			await this.client.connect();
		}

		const user = await this.client.HGET(`user:${userId}`, 'userSongs');
		return JSON.parse(user as string);
	}

	public async deleteUserSongs(userId: string) {
		if (!this.client.isOpen) {
			await this.client.connect();
		}

		await this.client.HDEL(`user:${userId}`, 'userSongs');
	}
}

export const userCache = new UserCache();
