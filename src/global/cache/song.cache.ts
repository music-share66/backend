import { BaseCache } from './base.cache';

export class SongCache extends BaseCache {
	constructor() {
		super('Song');
	}

	public async setSongM3U8Url(songId: string, songM3U8Url: string): Promise<void> {
		if (!this.client.isOpen) {
			await this.client.connect();
		}

		await this.client.SET(`song_M3U8Url:${songId}`, songM3U8Url);
	}

	public async getSongM3U8Url(songId: string): Promise<string> {
		if (!this.client.isOpen) {
			await this.client.connect();
		}

		const songM3U8Url = await this.client.GET(`song_M3U8Url:${songId}`);
		return songM3U8Url as string;
	}
}

export const songCache = new SongCache();
