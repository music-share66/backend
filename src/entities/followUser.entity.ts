import { Entity, PrimaryGeneratedColumn, CreateDateColumn, ManyToOne } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class FollowUser {
  @PrimaryGeneratedColumn('uuid')
	public id: string;

  @ManyToOne(() => User, user => user.following, { onDelete: 'CASCADE' })
  public follower: User;

  @ManyToOne(() => User, user => user.follower, { onDelete: 'CASCADE' })
  public followee: User;

  @CreateDateColumn({ type: 'timestamptz' })
  public createDate: Date;
}
