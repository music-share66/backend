import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm';
import { Song } from './song.entity';
import { FollowUser } from './followUser.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
	public id: string;

  @Column()
  public email: string;

  @Column({ type: 'text', nullable: true })
  public passwordHash: string | null;

  @Column()
  public userName: string;

  @Column({ type: 'text', nullable: true })
  public idToken: string | null;

  @Column({ type: 'text', nullable: true })
  public refreshToken: string | null;

  @CreateDateColumn({ type: 'timestamptz' })
  public createDate: Date;

  @UpdateDateColumn({ type: 'timestamptz' })
  public updateDate: Date;

  @OneToMany(() => Song, song => song.user)
  public songs: Song[];

  @OneToMany(() => FollowUser, followUser => followUser.follower)
  public following: User[];

  @OneToMany(() => FollowUser, followUser => followUser.followee)
  public follower: User[]; 
}
