import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, CreateDateColumn, UpdateDateColumn, OneToOne } from 'typeorm';
import { Song } from './song.entity';

@Entity()
export class SongStorageDetail {
  @PrimaryGeneratedColumn('uuid')
	public id: string;

  @Column()
  public storageType: string;

  @Column()
  public awsRegion: string;

  @Column()
  public awsS3BucketName: string;

  @Column()
  public fileDestination: string;

  @Column()
  public originalSongFileKey: string;

  @Column({ nullable: true })
  public encodedSongFileKey: string;

  @CreateDateColumn({ type: 'timestamptz' })
  public createDate: Date;

  @UpdateDateColumn({ type: 'timestamptz' })
  public updateDate: Date;

  @OneToOne(() => Song, song => song.songStorageDetail, { onDelete: 'CASCADE' })
  @JoinColumn()
  public song: Song;
}
