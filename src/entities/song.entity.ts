import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToOne, ManyToOne } from 'typeorm';
import { User } from './user.entity';
import { SongStorageDetail } from './songStorageDetail.entity';

@Entity()
export class Song {
  @PrimaryGeneratedColumn('uuid')
	public id: string;

  @Column()
  public songName: string;

  @Column({ nullable: true })
  public durationInMs: number;

  @Column({ nullable: true })
  public encodeJobId: string;

  @Column()
  public processingStatus: number;

  @CreateDateColumn({ type: 'timestamptz' })
  public createDate: Date;

  @UpdateDateColumn({ type: 'timestamptz' })
  public updateDate: Date;

  @ManyToOne(() => User, user => user.songs, { onDelete: 'CASCADE' })
  public user: User;

  @OneToOne(() => SongStorageDetail, songStorageDetail => songStorageDetail.song)
  public songStorageDetail: SongStorageDetail;
}
