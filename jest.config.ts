import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
	preset: 'ts-jest',
	testEnvironment: 'node',
	verbose: true,
	coverageDirectory: 'coverage',
	collectCoverage: false,
	testPathIgnorePatterns: ['/node_modules/'],
	transform: {
		'^.+\\.ts?$': 'ts-jest'
	},
	testMatch: ['<rootDir>/src/**/test/*.ts'],
	collectCoverageFrom: ['src/features/**/services/*.ts', '!src/**/test/*.ts?(x)', '!**/node_modules/**'],
};

export default config;
